## Unit Six

本单元主要是指路线和交通工具

### 词组与固定搭配

1.  `live in the city centre` 住在市中心
2.  `live in the suburbs` 住在郊区。【注】此处`suburb`==需要复数==
3.  `be quiet and peaceful` 安静和宁静的
4.  `on the map of` 在...的地图上
5.  `take a bus to sp. = go to sp. by bus` 乘巴士去某地
6.  `take the underground to sp. = go to sp. by underground`  乘地铁去某地。
    【注】==`underground`前采用定冠词==
7.  `tell us about your new neighbourhood`告诉我们关于你们的新街区
8.  `not much traffic` 没有许多车辆【注】`traffic`为不可数名词
9.  `at the bottom of` 在...的底部【注】此处使用`at`
10. `be noisy and exciting` 嘈杂和令人兴奋的
11. `be pleasant and relaxing` 令人愉快和放松的
12. `a financial centre` 一个金融中心
13. `an exhibition centre` 一个展览中心
14. `go to kindergarten` 去上幼儿园。【注】`kinder-garten`为德语，原意儿童们的花园，注意拼写
15. `watch stars at night` 在晚上看星星
16. `swim in the sea` 在海里游泳。【问】==为何此处要加定冠词？==
17. `read storybooks at home` 在家里看故事书
18. `go to a restaurant for dinner` 去饭店吃晚饭
19. `wear warm clothes and gloves`穿着暖和的衣服、戴着手套

#### 提示

1.  理解一下 `go to school/go to university/go home/in hospital` 等词汇中没有`the`的原因。注意这里没有定冠词的含义分别是：**上学，上大学，回家和住院**，代表大家都熟悉的意思，而不是去特定的学校，去特定的大学校园，去特定的家，去特指的一员。

### 词性转换

1.  `peace` n.平静 -> `peaceful` a. 平静的
2.  `interest` n.兴趣 -> `interesting` a.有趣的, `interested` a.感到有趣的
3.  `exhibit` v.展览 -> `exhibition` n.展览
4.  `neighbour` n.邻居 -> `neighbourhood`n.街区【拓】==留意英式和美式的区别==
5.  `exciting` a.令人兴奋的 -> `excited` a.感到兴奋的
6.  `please` v.使高兴 -> `pleased` a.高兴的, `pleasant` a.令人愉快的, `pleasure` n.高兴

#### 提示

1.  动词原型 `+ing / +ed` 后变成的形容词，通常`ing`用来形容动作客体，而`ed`用来形容动作主题被动作。
    【例子】
    `interesting` 有趣的(东西)；`interested` (人)感到有兴趣；`be interested in` 从某个角度来看是被动语态所衍生的形容词。
    `exciting` 令人兴奋的(东西)；`excited`（人）感到兴奋的。
    `pleasant` 没有ing形式；`pleased`（人）感到高兴的。
2.  英语许多词汇可以互相变化，请留言前缀/后缀对于词性转换的作用。
    前缀：`in/im/un/dis/de/uni/twi/tri/qua/mis/ge`
    后缀：`ly/ment/tion/er/or/ee`

### 语法与句型

1.  `Where is Water Bay on the map of Garden City?` 表示在地图上时用介词on.
2.  `It's in the north of the map.` 表示地图上的方位时用介词in。位to/in
3.  `I take a bus to Sunshine Shopping Centre. Then I take the underground to school.`
    `take + 交通工具 + 介词to` 意为搭乘….去某地，一般`bus,car, taxi`前用`a`， 而在`underground`前用`the`。可与`go to...by + 交通工具` 互相替换。
    本例可改写为`I go to Sunshine Shopping Centre by bus. Then I go to school by underground.`
    【问】`How do you go to school?` 这里留意冠词`a/an/the/免冠词`的用法。
4.  `Is it convenient to go shopping in your new neighbourhood?`
    `It is + 形容词 + to do sth.` 表示做某事很怎么样。`it`不是句子的真正主语，它代替了`to do
    sth.`的部分，称之为虚拟主语或形式主语。动词不定式部分为真正主语。
5.  `It's winter because people are wearing warm clothes and gloves.`
    这是`because`的引导原因状语从句。==原因状语从句引导词还可以是`for`==。
6.  选择疑问句:`Do you live in the city or the suburbs?`
    回答时用肯定句。`I live in the city. | I live in the suburbs.`
7.  `When we lived in the city centre, we got up late.`
    由`when`引导的时间状语从句，用**一般过去时论述过去经常发生的动作**。
    【拓】主将从现：许多状语从句，主语采用一般将来时，而从句采用一般现在时。这需要专题讨论。
8.  现在进行时：
    概念：表示现在正在进行或发生的动作。
    基本用法：寻找关键词如`now,listen!,look!,look out!It's 7:00`。
    动词特征：`be(am/is/are)` + 动词的现在分词
    【例】
    `What are you doing now?`
    `It's spring. Birds are making nests in the trees.`
    【辨】==区分主系表结构采用`ing`形式的形容词：==`It is interesting.`(一般现在时)
    【拓】==`be`动词也有现在进行时，此时`be`代表存在，是实义动词。==
    【拓】`To be or to be, that is a question.` ==莎士比亚《哈姆雷特》==
    `She is being stubborn.` 她在此事上很固执。
    `She is subborn.`她一直很固执。

## Unit Seven

本单元主要教了交通标志。

### 词组与固定搭配

1.  `signs around us` 我们周围的标志
2.  `a direction sign` 一个方向标志
3.  `a warning sign` 一个警告标志
4.  `an information sign` 一个信息标志
5.  `an instruction sign` 一个说明方法的标志
6.  `turn left /right` 向左/右转
7.  `have a barbecue` 进行（一次）烧烤
8.  `leave rubbish` 扔垃圾
9.  `tell sb to do sth. / tell sb.not to do sth.` 告诉某人做/不要做某事
10. `use the telephone for help` 打电话求救
11. `go camping` 去野营
12. `have a picnic` 去野餐
13. `get useful information` 得到有用的信息
14. `fly kites` 放风筝
15. `park our car` 停车
16. `wash these clothes in warm water` 在温水里洗这些衣服
17. `pick the flowers` 摘花
18. `keep quiet` 保持安静
19. `roll the dice` 扔骰子
20. `take turns to do sth.` 轮流做某事
21. `land on` 着陆
22. `miss a turn` 轮空一次
23. `good luck` 祝你好运
24. `in a park` 在公园里
25. `in the countryside` 在乡间
26. `on a road` 在马路上
27. `at school` 在学校

【注】以上各种习惯用语中，留意`a/an/the/免冠词`的搭配。

### 词性转换

1.  `direction` (n.)方向；方位 -> `direct` (v.)指导 `director` (n.)导演
2.  `warning` (adj.)警告:警示 -> `warn` (v.)警告
3.  `instruction` (adj.)说明用法的；操作指南的 -> `instruct` (v.)指示
4.  `silence` (n.)寂静；无声 -> `silent` (adj.)安静的；寂静的
5.  `useful` (adj.)有用的；实用的 -> `use` (v.)/(n.)使用 `useless` (adj.)无用的
6.  `mean` (v)意味着 -> `meaning` (n.)意思 `meaningful` (adj.) 有意义的
7.  `information` (n.) 信息 -> `inform` (v.)通知；告知
8.  `player` (n.)游戏者 -> `play` (v)玩/(n.)戏剧
9.  `person` (n.)人 -> (pl.) `people` (n.)人们 `persons`
10. `luck` (n.)运气 -> `lucky` (adj,)幸运的 `luckily`(adv.)幸运地
    `unlucky` (adj.)不幸的 `unluckily`(adv.)不幸地
11. `different` (adj.) -> 不同的`difference` (n.)差别
12. `fly` (v.)飞 -> `flight` (n.)飞行；航班
13. `miss` (v)错过 -> `missing` (adj.)失踪的
14. `decide` (v.)决定 -> `decision`(n.)决定
15. `sign` (n.)标识 -> `signature`(n.)签名

### 语法与句型

#### 句型

1.  情态动词(modal verb)用法【注】情态动词是助动词的一种。
    `can, may, must,need, should, would` 以及 `ought,could`
    肯定句：情态动词 + 动词原形
    【例】`We may have a barbecue this Sunday.`
    否定句:情态动词 + not + 动词原形
    【例】`You mustn't talk in class.`
    一般疑问句：情态动词 + 主语 + 动词原形
    肯定回答：`Yes`, 人称代词+情态动词
    否定回答：`No`，人称代词+情态动词+ not
    【例】`Can I eat the cake on the table? Yes, you can. No, you can't.`
    【拓】`should/must/shall`等提问时，会有==固定搭配==，并==注意区分语气差异==
    【例】`Must I ... ? No, you needn't. | No. You don't have to.`
2.  `there be`结构
    【注】`There be` 存在动词的就近原则，即`be`动词的数与离它最近的一个主语保持一致。
    【例】
    `There is a bottle of milk and some apples in the fidge.`
    `There are some apples and a bottle of milk in the fridge.`
3.  `What kind of sign is this?`
    `It is a direction sign.`
    `It is a warming sign.`
    `It is an information sign.`
    `It is an instruction sign.`
4.  `Where can we find it?`
    `In a park. / In the countryside. / On a road. / At school.`
    【注】==留意地点前面的介词。熟读背诵！==

#### 语言点

1.  `What does this sign mean?` = `What is the meaning of this sign?`
2.  `This sign tells us how and where to go.`
    a. `tell sb to dol tell sb not to do` (同类型`promise,ask`)
    【例】`My mother often tells me not to give up.`
    b. 疑问词 + 动词不定式
    此结构通常放在`ask, know, learn, teach, tell`等动词之后，用作宾语。
    【例】`I lost my way. I really don't know which bus to take.`
3.  `This sign tells us how and where to go. It is a direction sign.`
    `This sign tells us things we must not do. It is a warning sign.`
    `This sign tells us things we may want to know. It is an information sign.` `This sign tells us how to do something. It is an instruction sign.`
4.  `We can go camping here.`
    `go + doing` 去干…
    【例】`go cycling, go swimming, go shopping, go fishing, go hiking`
5.  `We can use the telephone for help.`
    a. use sth. to do=use sth for doing./sth
    【例】
    `Firemen use long ladder to rescue people from tall buildings.`
    \= `Firemen use long ladder for rescuing people from tall buildings.`
    b. `use the telephone for help`
    `help`为名词，`ask for help`请求帮助
    `help`也可作为动词，`help sb.(to) do sth. / help sb.with sth.`
    【例】`He is so kind that he usually helps the old cross the road safely.`
    `My brother is good at maths. So he sometimes helps me with my maths lessons.`
6.  `We must not pick the flowers here.`
    `pick the flowers` 摘花
    `pick up` 捡起 【例】`pick up rubbish`
7.  `We must not smoke here.`
    【例】`Would you mind if I smoke here?` `smoke` v.抽烟
    【例】`The factories sent out a lot of smoke.` `smoke` n.烟
8.  `We must keep quiet.`
    `keep`在这里作系动词使用，后面接形容词(同类型`make`和感官动词`look，taste, smell,sound, feel`) 【例】`The death of the president made the people all over the country sad.`
    `keep (sb.) doing sth.` (让某人)一直做某事
    【例】`She keeps studying English for 1 hour every day.`
    `keep sb.from.….`阻止(不让)某人做某事
    `Faith(信仰) can keep people from doing bad things.`
9.  `We can get useful information here.`
    `information`不可数名词(同类型`news,advice`) 一条信息`a piece of information`
10. `The player who rolls a six on the dice goes first.`
    定语从句意为:掷骰子掷到六的人先进行比赛。
11. `Then all the other players take turns to roll the dice.`
    a. `take turns to do` = `do..by turns` 轮流，依次
    【例】`Do they take turns to look after that puppy?` = `Do they look after that puppy by turns? `
    b. `turn` 在句中作名词，意为(依次轮到的)机会
    `turn`还作为动词,`turn left` 向左转
12. `If you land on a sign, you must say what it means.`
    `land` v.落，降落
    【例】`In ten minutes the plane will land in Shanghai.`
    `land` n.陆地
    【例】`Elephant is the largest animal on land.`
13. `If you don't know, you miss a turn.`
    `miss` v.错过
    【例】`I missed the school bus because I got up late this morning.`
    `miss` v.想念
    【例】`My father is working in another city. I miss him so much.`
14. 地点的表达
    `in a park / in the countryside / on a road / at school` 留意介词

## Unit Eight

本单元主要学习了运动与健康。

### 词组与固定搭配

1.  `diet and health` 饮食和健康
2.  `grow healthy` 健康成长
3.  `grow strong` 茁壮成长
4.  `spend one's weekend` 过周末
5.  `at Health Camp` 在健康度假营
6.  `go swimming` 去游泳
7.  `in the swimming pool` 在游泳池里
8.  `Neither do I.` 我也不。
9.  `So do I.` 我也是。
10. `play badminton` 打羽毛球
11. `read magazines` 读杂志
12. `in the reading room` 在阅览室
13. `in the hall` 在大厅里
14. `a cartoon called.…`一部名为.….的卡通片
15. `exciting and interesting` 既激动又有趣
16. `That's a good idea.` 那真是个好主意
17. `our outing` 我们的远足
18. `an information sheet` 一张信息纸
19. `in the camp` 在度假营里
20. `stay healthy and strong` 保持健康和强壮
21. `to be a healthy child` 做一个健康的孩子
22. `go to bed early` 早睡
23. `get up early` 早起
24. `exercise regularly` 定期锻炼
25. `at least` 至少
26. `eight glasses of water` 八杯水
27. `too much sweet food` 太多的甜食
28. `too many soft drinks` 太多的软饮料
29. `good/ bad habits` 好/坏习惯
30. `have a sore throat` 喉咙痛
31. `have toothache` 牙痛
32. `have a stomach ache` 胃痛
33. `last Sunday` 上周日
34. `three packets of crisps` 三包薯片
35. `two boxes of ice cream` 两盒冰淇凌
36. `some vegetables` 一些蔬菜
37. `four bars of chocolate` 四块巧克力
38. `three bottles of lemonade` 三瓶柠檬汁
39. `a lot of fried food` 许多油炸食品
40. `enough fruit` 足够的水果
41. `watch less TV` 少看电视
42. `eat fewer crisps` 少吃薯片
43. `eat more fruit` 多吃水果
44. `change my bad habits` 改变我的坏习惯
45. `used to do sth.` 过去常常做某事
46. `not...any longer.` 不再
47. `do a lot of exercise regularly` 有规律地做许多的运动
48. `study hard` 努力学习
49. `in the past` 在过去

### 词性转换

1.  `health` n.健康`healthy` a.健康的`unhealthy` a.不健康的
2.  `call` v. 叫喊；呼唤；打电话`called` a.被叫做……的，名为…的
3.  `regularly` adv.有规律地；正常地`regular` adj.有规律的；正常的
    `irregularly` adv.无规律地；不正常地`irregular` adj.无规律的；不正常的
4.  `many/much` adj./pron.许多的/许多`more` adj./pron.更多的/更多`most`最多的
5.  `few` adj. 少数的；几乎没有的；pron. 少数:几乎没有
    `fewer` adj.较少的;pron. pron.较少
    `fewest`最少的(用于可数名词)
6.  `litle` adj.少的`类似some`;adv.少;n.没有多少
    `less` adj.较少的;adv.较少地n.更少的数
    `least` adj.最少的
7.  `change`v.变化，交换`change` n.变化；零钱,`changeable` adj.易变的，不定的
8.  `suggest` v.建议 `suggestion` n.建议

### 语法与句型

1.  `Mr Hu and the students are spending their weekend at Health Camp.`
    胡老师和学生们在健康营度周末。
    `spend one's weekend`意为“度周末”。`spend`除了“花费(金钱)”之外，还有“度过时间”的意思。这背后的隐喻就是时间就是金钱。
    【例】`Let's spend a day out together tomorrow. Where are you going to spend your holiday?`
    `spend time/ money on sth./(in) doing`
2.  提建议的方式:
    `Shall we do..…?`
    `Let's do.…`
    `How about doing..?`
    `What about doing.?`
    `Why not do…?`
    `Why don't we do..?`
    应答:`That's a good idea.`
3.  `So do I./Neither do` 我也是/我也不。
    `So, neither`引导倒装句。`So.…I`表示同意对方的话语，意为“我也是”，上文为肯定句。`Neither…I`也用来附和对方的话语，意为“我也不”，上文是否定句。应注意在`so，neither`后面的助动词，情态动词和be动词以及其后的主语在人称、时态上应与上一句保持一致。
    【例】`Mary can paint well. So can I.`
    `My brother is a doctor. So is Lisa's.`
    `Mike doesn't like reading stories. Neither does Peter.`
    `I have never been to Beijing. Neither has Kitty.`
4.  `neither`用法小结
    a. 用作副词，表示“也不’常用于倒装句的开头，紧跟在一个否定句后。`John can't swim. Neither can I.`
    b. 用作形容词，表示“(两者)都不”，置于单数名词之前。`Neither watch is made in China. `
    c. 用作代词，表示“两者都不；双方均不”。`He answered neither of the letters. `
    d. 用作连词，常用短语`neither..nor.…`表示“既不……·也不。`Neither he nor I am well educated.` (谓语动词就近原则)
5.  `There's a cartoon called 'Computer War'.` 有一部名叫“电脑大战”的动画片。
    `called` adj.意为“被叫做……,名叫…”。`I have a friend called Happy.`
6.  `If you want to stay healthy, you should go to bed early and get up early.` 如果想保持健康，你就应该早睡早起。
    if引导的条件状语从句:从句通常用一般现在时，主句通常用一般将来时(==主将从现==)。`I will call you if he comes back tomorrow. You will miss the train if you don't hurry.`
7.  `stay healthy` 保持健康
    `stay`在这里为系动词，意为“保持”，与`keep`意思相近。`stay healthy = keep healthy`
8.  `go to bed`去睡觉，指上床睡觉的动作及过程，不一定睡着。
    `go to sleep`入睡，睡着
9.  `get up`起床
    `wake up`醒来，人醒了并不一定就会从床上起来。
10. `should` 情态动词，后跟动词原形
    肯定:`should do`
    否定:`should not/ shouldn't do`
    一般疑问:`Should..do.?`
11. `at least` 至少
    【近】`no less than`不少于；多达【反】 `at most` 至多
12. `every day`每天，天天 是副词短语，常用作时间状语。
    【例】`She goes to school every day.`
    `everyday` 每天的，日常的，平凡的是形容词，常用来修饰名词，作定语。
    【例】This is a matter of everyday occurrence. ×
13. `used to do.`过去常常做某事
    【例】`We used to go there every year.` `used to`的否定形式有两种: `usedn't to`或`didn't use to`，后者更加常见。
14. `not….any longer` = `no longer` 不再 `not any more`.
    【例】`They don't live here any longer`
    `They no longer live here.`
    `They don't live here any more.`

## Unit Nine

本单元主要学习了各国的食品和食物。

### 词组与固定搭配

1.  `different foods`  不同的食物
2.  `need sth to do sth`  需要某物做某事
3.  `take care of` 照顾
4.  `homeless animals` 无家可归的动物
5.  `raise some money for…` 为……….筹钱
6.  `have an international food festival` 举行一个国际食品节
7.  `sell food from different countries` 销售来自不同国家的食物
8.  `in the playground` 在操场上
9.  `make different foods` 制作不同的食物
10. `ask sb to do sth` 请求某人做某事
11. `on Saturday`  在星期六
12. `at ten o'clock` 在十点整
13. `in the morning` 在早上
14. `like sth. /sb. best` 最喜欢(某物/某人)
15. `Chinese rice puddings` 八宝饭
16. `moon cakes` 月饼
17. `rice dumplings` 粽子
18. `hot dogs` 热狗
19. `apple pies` 苹果派
20. `raisin scones` 葡萄干
21. `fish and chips` 烤饼鱼与薯条
22. `pineapple fried rice` 菠萝炒饭
23. `prawn cakes` 虾饼
24. `Chinese/American/English/Thai food` 中国的/美国的/英国的/泰国的食品
25. `show sb.how. to make sth.`教某人如何制作某物
26. `of course` 当然
27. `would like to do sth. /want to do sth.` 想要做某事
28. `200 grams of self-raising flour` 200克自发面粉
29. `mix A,B and C together` 把A，B和C混合在一起
30. `pour into sth.` 倒入某物
31. `make sth. into sth.` 把某物制作成某物
32. `a little`一点(+不可数名词)
33. `150 millilitres of milk` 50毫升牛奶
34. `make shapes` 制成形状
35. `about...centimetres wide` 大约..厘米宽
36. `put…on a baking tray` 把.…放入一个烤盘中
37. `sprinkle A with B` 把B撒入A
38. `bake sth.in an oven` 把某物放入烤炉中烤制
39. `at the intermational food festival` 在一个国际食品节中
40. `….yuan and….jiao` ….元.角
41. `a plate of /a bowl of` 一盘/一碗
42. `May I have…….please?`  我可以拿……吗？
43. `be very well` 非常好(健康)
44. `at the SPCA` 在SPCA
45. `think about`  考虑；思考
46. `hear from sb.` 收到某人的来信

### 词性转换

1.  `home` (n.)家 -> `homeless`(adj.)无家可归的
2.  `different` (adj.)不同的  -> `difference` (s)(n.)不同
3.  `nation` (n.)国家，民族 -> `nationality`(n.)国籍 -> `national` (adj.)国家的
    `international` (adj.)国际的
4.  `care` (v.)照顾；照料 -> `careful` (adj.)小心的，仔细的 -> `carefully` (adv.)小心地；仔细地
5.  `help`(v.)帮助 -> `helpful` (adj.)助人的，乐于助人的
6.  `funny` (n.)滑稽的，可笑的 -> `fun`(n.)乐趣)
7.  `China`(n.)中国 -> `Chinese`(adj.)中国的(n.)中国人
8.  `England`(n.)英格兰 -> `English` (adj.)英国的 (n.)英国人
9.  `America`(n.)美国 -> `American` (adj.)美国的(n.)美国人
10. `Thailand` (n.)泰国 -> `Thai`(adj.)泰国的(n.)泰国人)
11. `fry`(v.)炒；炸；煎 -> `fried`(adj)油煎的
12. `mix` (v.)混合 -> `mixture`(n.)混合物
13. `bake` (v.)烘烤 -> `baking` (n.)烘烤 `a baking tray` `baker`(n.)面包师 -> bakery(n.)面包店
14. `final` (adj.)最后的 -> `finally` (adv.)最后
15. `sincere`(adj.)真诚的 -> `sincerely` (adv.)真诚地
16. `one` (num.) 1 -> `first/ firstly`(adv.)首先；第一
17. `two` (num.)二 -> `second / secondly` (adv.)第二；其次

### 语法与句型

#### 同义词

1.  `take care of` = `look after` = `care for`照顾；照料
    【例】 `You should take care of your health.`
    `You should look after / care for your health.`
2.  `have` = `hold`举行；举办
    【例】`We are going to have a sports meeting tomorrow.`
    `We are going to hold a sports meeting tomorrow.`
3.  `like sth. best` = `favourite` 最喜爱
    【例】`I like English food best.` = `English food is my favourite.`
4.  `finally` = `at last`最后
    【例】`Finally,Jolin won the quiz game.` = `At last,Jolin won the quiz game.`
5.  `would like to do sth.` = `want to do sth.`想要做某事
    【例】`Peter would like to sell Thai food.` = `Peter wants to sell Thai food.`
6.  `How much do they cost?` = `How much are they?`他们值多少钱？
    `How much does it cost?` = `How much is it?`它值多少钱？
7.  `hear from sb.` = `receive a letter from sb.` 收到某人的来信
    【例】`It was a pleasure to hear from you.`
    `It was a pleasure to receive a letter from you.`

#### 一词多义

1.  `hear`词义
    a. 表示“听见” 【例】`I can't hear you clearly.`
    b. 表示“听说” 【例】`I heard that he was ill. `
    c. hear from表示“收到某人的来信” 【例】`I hear from my cousin every two weeks.`
2.  `raise`词义
    a. 表示“升起；举起”【例】 `We raise the Chinese national flag at school every day.`
    b. 表示“募集”  【例】 `We need to raise some money for the homeless people in Wenchuan.`
    c. 表示“饲养” 【例】`My uncle raise some sheep on his farm.`
3.  `ask`词义
    a. 表示“问” 【例】`May I ask you a question? Of course.`
    b. 表示“请求”【例】`We have asked the Smiths to come for dinner on Friday.`

#### 语言点:

1.  介词
    a. 时间介词:
    `at` + 具体时刻: 【例】`at five o'clock`
    `in` + 上午/下午/傍晚 【例】`in the morning/in the afternoon`
    `on` + 星期/日期【例】`on Saturday / on 2 February `
    `for` + 一段时间 【例】`for 15 minutes` (注意:使用`How long`提问)
    b. 地点介词:
    `in`: `in the playground`
    `at`: `at the SPCA `
    c. 其他:【例】`at 200 C`

#### 句型

1.  表示一般将来时:`be going to do`
    【例】`We're going to raise some money for the SPCA.`
    `When are we going to have it?`
2.  表示“让我们做…"`Let's do sth.`=`Shall we do.?`
    【例】`Let's have an internmational festival in the playground.` 【注】回答可用:`That's a good idea.`
3.  情态动词:`Can:Can you.…?`你可以..吗？
    【例】`Can you show me how to make a chocolate cake?`
    回答:`Of course.Certainly.` `May: May I.?` = `Can I..?`我可以….吗？
    【例】`May I have a plate of fried food?`
    回答:`Yes,of course.`
4.  祈使句:
    肯定句:`Do(sth).`【例】`Mix butter, flour and sugar together.`
    否定句:`Don't do (sth)..`【例】`Don't put your untidy gloves on the bed, please.`

#### 各国的风味食物

| Country | Food          | Food                                      |
| ------- | ------------- | ----------------------------------------- |
| China   | Chinese food  | rice puddings/ moon cakes/ rice dumplings |
| America | American food | hot dogs/ apple pies/ hamburgers          |
| England | English food  | raisin scones/ fish and chips             |

## Unit Ten

### 词组与固定搭配

1.  `prepare for` 为….做准备
2.  `It sounds great!`这听上去很棒！
3.  `look forward to sth. / doing sth.` 期盼某物/做某事
4.  `be free`有空
5.  `at my flat` 在我的公寓
6.  `What a pity!`多可惜啊！
7.  `at three o'clock in the afternoon` 在下午三点钟
8.  `on Saturday afternoon` 在星期六下午
9.  `sing Karaoke` 唱歌
10. `play chess` 下棋
11. `see you then` 到时候见
12. `have got something to do`有些事要做
13. `need sth./to do sth.` 需要某物/做某事
14. `show sb.how to do sth.` 展示给某人看如何做某事
15. `icing sugar` 冰糖
16. `add A to B`把A加入到B
17. `mix...together`把…混合在一起
18. `stir the mixture`搅拌混合物
19. `put..in an oven`放入烤箱
20. `bake it for 20 minutes at 180c`调至180度考上20分钟
21. `a baking tray`一个烤盘
22. `wait for`等待
23. `on the top of`在..的顶部
24. `Firstly,Secondly,Thirdly,Next,Then.After that,Finally,…`首先，其次，第三，然后，接下来，之后，最好
25. `Would rather do/not do` 宁愿做/不做
26. `ask sb.about sth.`询问某人关于某事请
    `ask sb.to do sth.`求某人做某事
27. `buy sth.for sb./buy sb.sth.`为某人买某物
28. `write-wrote-written`写
    `come came come`来
    `have-had-had`举办
    `sing-sang--sung`唱歌
    `give-gave一given`给
    `teach-taught-taught`教
29. `write an e-mail to sb.`写给…一封邮件
30. `tell sb.about sth./to do sth.`告诉某人某事/做某事
31. `give sb.sth./sth.to sb.`给某人某物
32. `teach sb.(how) to do sth.`教某人做某事

### 词性转换

1.  `prepare` v.准备 -> `preparation` n. 准备
2.  `mix`混 -> `mixture` n.混合物
3.  `ice` n. 冰 -> `icing` n.糖衣
4.  `battery`n.电池 -> `batteries` n.(pl.)电池复数
5.  `match` n. 火柴 -> `matches` n.(pl.)火柴复数
6.  `decorate` v. 装饰 -> `decoration` n.装饰

### 语法与句型

1.  时态:一般将来时 `be going to /will do…`打算做/将要做
    【例】`What are you going to do at the party?`
    【例】`I'll see you on Saturday aftermoon at three o'clock at my flat.`
2.  日常会话:
    打电话常用语:
    【例】`Hello. This is Kitty.May I speak to. Peter,please?`
    【例】`Hello,this is Peter speaking.`
    日常问候用语:
    【例】`How are you?-I'm fine, thanks.`
    表示祝愿用语:
    【例】`Have a great party.-Thank you.`
    告别用语:
    【例】`Bye. See you then/ later/ soon.`
3.  `sound +adj.` 听上去…连系动词+形容词，之前有学过 `keep, look, taste, feel`等系动词。
4.  句型:用祈使句发指令。`Firstly, make the icing.`
5.  `What ingredients do we need?`我们需要哪些原料？ `need sth./to do sth.`实意动词；否定式是`助动词+not +need`。要注意的是作为情态动词`need+v-原形`；否定形式是`needn't`
6.  `To make a chocolate cake,..To make…`为了要做.提问用`why`

