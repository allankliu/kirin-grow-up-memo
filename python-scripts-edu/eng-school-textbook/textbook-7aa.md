## Foreword

彭浦初级中学的英语教研室为同学们准备了校本。主观上是为同学们提炼了英语的学习精华。但“道不轻授”，部分同学们脱离课本，不再记录课堂笔记。客观上同学们更加“懒”了。此外，同学们看到的是提炼后的语法项，事实上割裂了对于原始上下文的理解和语感。

这么说吧，校本如同一把屠龙刀。可是这把刀必须要自己锤炼才是屠龙刀，否则就是一把菜刀。

为此，我在彭初的校本基础上做了如下增改。首先扩充了我对于内容的校注。同时通过配套的Python程序集和AI平台的能力来带动孩子们互动参与。相关内容有：

1.  **连词成句**（Python），会从课本、校本中随机抽取句子要求重新连接。
2.  **首字母填空**（Python），会随机从英语内容库中，抹去需要填空的部分。
3.  **单词联想**（Python），会随机从旅行、国家、宠物、学校、音乐、艺术、科学等，汇集100\~200个相关名词、动词、形容词、词组等。构成孩子自己的语库。
4.  **根据校本内容**（AI），利用AI平台对词组、固定搭配以及语法点，进行专项选择、填空题，暂定清华大数据服务。
5.  **命题写作**（人工/AI纠错），根据课文内容，要求场景化写作。
6.  **课文英译中后中译英**（人工/自习），进行核对。
7.  **歌曲的中英互译填词**，(人工/AI辅助)，提高孩子兴趣。

欢迎学有余力，具备基础文档管理和编程能力的孩子加入。

## Unit One

本单元主要是旅行与亲戚之间的交往，请留意相关词汇、固定搭配。

### 词组与固定搭配

| English                             | 译文            | 备注                                     |
| :---------------------------------- | :------------ | :------------------------------------- |
| get a letter from                   | 从某人处收到一封信     | = receive a letter from sb.            |
| write a letter to                   | 写封信给…         | = write sb. a letter                   |
| send us a photo                     | 寄给我们一张照片      |                                        |
| talk to Mum                         | 与妈妈谈话         |                                        |
| talk about the trip                 | 讨论旅行          |                                        |
| travel agent                        | 旅游代理人         | agent为代理人，agency为代理机构                  |
| At the travel agent's               | 在旅行社          | ==‘s后省略了office ?==                     |
| at the end of August                | 在八月末          | 固定搭配，多练几遍                              |
| The Summer Palace                   | 颐和园           |                                        |
| The Tian'anmen Square               | 天安门广场         | Shaanxi 陕西，shanxi 山西，aa/a用来区分，注意原因前的'号 |
| The Great Wall                      | 长城            |                                        |
| The Palace Museum                   | 故宫            | ==以上四个特定的地点都需要定冠词the==                 |
| in the north-west of Beijing        | 在北京的西北面       |                                        |
| north of Beijing                    | 在北京的北面        |                                        |
| build it with bricks and stones     | 用砖和石头建造它      |                                        |
| a long wall with towers on it       | 带有塔的长墙        |                                        |
| in the centre of Beijing            | 在北京的中心        |                                        |
| the history of China                | 中国历史          |                                        |
| places of interest                  | 名胜古迹          | ==interest这里是单数！==                     |
| interesting places                  | 有趣的地方         |                                        |
| be back(come back)                  | 回来            |                                        |
| a photo of you and me               | 一张我和你的照片      | ==me一般在最后==                            |
| invite us to stay with his family   | 邀请我们和他的家人小住   |                                        |
| on 16 August                        | 在八月十六日        |                                        |
| see swans swimming                  | 看见天鹅游泳        |                                        |
| hold one million people             | 容纳一百万人        | ==注意hold在此的用法，留意hold在其他地方的用法==         |
| a place with many ancient buildings | 一个有着很多古老建筑的地方 |                                        |
| raise the national flag             | 升国旗           |                                        |
| have a wonderful time               | 玩的很高兴         | = have a good time                     |
| in front of                         | 在….前面         | ==留意收集英语中不同位置方位的表达法==，可在小红书知乎中寻找       |
|                                     |               |                                        |

### 词性转换

1.  `invite` v.邀请 -> `invitation` cn.邀请函，请柬
2.  `expensive` adj.昂贵的 -> `expense` cn./un. 费用，
    【注】费用和成本时为可数名词，代价损失时为不可数名词
3.  `agent` n.代理人；经纪人(人）-> `agency` n. 代理处(公司机构)
4.  `national` n.国家的 -> `nation` adj.民族；国家
    `nationality` n.国籍 `international` adj.国际的，
    【注】`inter` ==前缀，彼此之间，== 如`interactive，internet, interview`
5.  `interest` n.吸引力:趣味 -> `interested` adj. (人)对.…感到有趣味的
    `interesting` adj. (物)有趣味的
6.  `wonderful` adj.精彩的；令人高兴的 -> `wonder` n.奇迹；v.想知道
7.  `another` pron.另一(事物或人) -> `other` pron./adj. 其它的 【注】留意两次区别
8.  `history` n. 历史 -> `historical` adj.历史上的，实际发生过的(有关历史的事实)

### 语法与句型

1.  `I've got a letter from Uncle Weiming.` 我收到一封来自伟明叔叔的信
    `get a letter from sb.` = `receive a letter from sb. / hear from sb`
    【例】`My sister got a letter / received a letter / heard from her penfriend yesterday.`
2.  `He has invited us to stay with his family in August.` 他邀请我们在八月与他的家人待在一起
    `invite sb. to do sth.`邀请某人做某事
3.  `Are we going to travel to Beijing by plane?` 我们乘飞机去北京吗？
    `travel/go to sp. by plane` = `fly to sp.` = `take a plane to sp. by plane / by air`
    【例】
    `Are we going to travel to Beijing by plane?`
    `Are we going to take a plane to Beijing?`
4.  `Let's talk to Mum and Kitty.` 让我们与妈妈和kity讨论下。`talk to / with sb. about sth.` 与某人讨论某事
    【例】`talk with Mum about the trip`
    【注】`talk to`指==单方面谈话==，`talk with`指==双向沟通讨论==
5.  `We'd like to travel to Beijing by plane.`我们想乘飞机去北京旅行
    `would like (sb.) to do sth.` = `want(sb.) to do sth.` 想要(某人)做某事
6.  `How long does it take to travel from Garden City to Beijing by plane?` 乘飞机从花园城市到北京要多久？
    `how long` 时间多久，主要用来提问一段时间。
    `from...to...` 从.….到…
7.  `How much does it cost?`它多少钱？= `How much is it?`
    表达花费钱的句型
    `It/sth cost (sb) money`【例】`The trip costs me 1000 yuan`
    `Sb spend money on sth`【例】`I spend 1000 yuan on the trip`
    `Sb pay money for sth` 【例】`I pay 1000 yuan for the trip`
    【注】`spend on`和`pay for`的介词。
8.  `We're going to visit Beijing on 16 August.` 我们将在8月16日游览北京。
    `on 16 August` = `on August 16/16th` 读作:
    `on August the sixteenth`/`on the sixteenth of August`
    【注】==日期的书写与口头表达存在差异。==

    `on` 做时间前介词表示:
    具体的单日和一个特定的时间，如某日、某节日、星期几等。
    【例】`On Christmas Day (On May 4th), there will be a celebration.`
    在某个特定的早晨、下午或晚上。
    【例】`He arrived at 10 o'clock on a cold night.`
9.  `We're going to come back at the end of August.`我们要在八月底回来
    `come back`=`return`=`be back`意为“返回”
    `at the end of`在…末 【例】`at the end of the week`, `at the end of the street`
10. `How are we going to get there?` 我们打算怎么到那儿？
    `get to sp.`=`reach sp. `=`arrive at sp.` (具体地点)/`in sp.`(大地点:国家、城市、地区)
    【注】在`here, there, home`副词前省略to。【例】`get there/ home`
11. `Thank you for your letter and ...`感谢你的信以及...
    `thank sb.for sth. /doing sth.`因...感谢某人【例】`Thank you for your help.`=`Thank you for helping me.`
12. `The Li family has arrived in Beijing.`李先生一家刚到北京。`The Lis`=`The Li family`李家
    `the`+姓氏的复数形式意为“姓……一家人”:
    【例】`the Smiths`=`Mr and Mrs Smith(and children)`史密斯夫妇(及孩子们)
13. `Simon and Lucy want to take Ben and Kitty to some interesting places.` Simon和Lucy 想带Ben和kitty去一些有趣的地方
    `take sb./ sth. to sp.`带某人/某物去某处(从说话处带到别处)
    `interesting places`有趣的地方
    `places of interest`名胜古迹【注】`interest`==这里是单数！==
14. `Tourists can see swans swimming on the lake.` 游客可以看到天鹅在湖上游泳
    `see sb./sth.doing sth.` 看到某人/某物在做某事
    【例】`I saw them playing football when I passed the playground.`
15. `It can hold more than one million people.`它能容纳超过一百万人
    `more than`=`over` 【反】`less than`
    `one million`一百万，
    【注】具体数字表达时`hundred, thousand, milion`不加s，【例】`two hundred, three thousand`。而前面没有具体数字时，加s，【例】`hundreds of`,`thousands of`
16. `The children are planning to visit different places of interest in Beijing.` 孩子们正计划参观北京不同的名胜古迹
    `plan to do sth.`=`be going to do`计划做某事
17. `Ben and I had a wonderfiul time.`Ben和我都玩得很高兴。
    `have a wonderful/good time`=`enjoy oneself`=`enjoy one's time`=`have a lot of fun`
18. `another` adj.另一个(所存在的事物多于两个)
    【例】`Would you like another orange?`
    `I don't like the shirt. Would you please show me another one? `
    `one….the other`另一个(两者中另一个)
    【例】`I have two uncles. One is in Japan, the other is in England.`

#### 重点

1.  `I have got a letter from Uncle Weiming.`
    `He has invited us to stay with his family in August.`
    `I haven't seen my cousins before.`
    上述三个句子使用了现在完成时，
    **构成方式**
    a. 陈述句：主语 + `has/have` + v(过去分词)
    b. 否定句：在`has, have`后加`not`【例】`I haven't seen the film.`
    c. 疑问句：将`has`和`have`提前。【例】`Have you seen the film?`
    **基本用法**
    a) 现在完成时常同`already,just,yet,ever,never`等状语连用，表示影响存在。
    b) 也同“`for`+时间段”或“`since`+时间点”的状语连用，表示持续到现在的动作或状态。
    【例】`She has taught in the school for ten years. He has worked here since 2003.`

## Unit Two

本单元主要是动物救助方面的内容，注意积累动物方面的词汇，包括宠物，家禽，和野生动物。

### 词组与固定搭配

|    | English                                               | 中文            | 备注                                                                                                                                   |
| :- | :---------------------------------------------------- | :------------ | :----------------------------------------------------------------------------------------------------------------------------------- |
| 1  | an SPCA officer                                       | 一位SPCA官员      | an 中的 ==n 是根据后续元音发音来隔断两个元音的==，留意一些特殊情况，如**缩写词汇发音是元音的**如E/S，但是如果==缩写构成了新的词汇以辅音开头则必须使用 a==\*\* \*\*，如SPICE。以及==元音字母开头，却是以辅音发音==的，如use。 |
| 2  | the Society for the Prevention of Cruelty to Animals  | 爱护动物协会        |                                                                                                                                      |
| 3  | That's right.                                         | 对的。           | 用于肯定                                                                                                                                 |
|    | That's all right.                                     | 没关系。          | 用于回应别人道歉                                                                                                                             |
|    | All right.                                            | 好的。           | 用于同意                                                                                                                                 |
|    | You're right.                                         | 你是对的。         | 承认对方观点正确                                                                                                                             |
| 4  | leave ... in the street                               | 将.….遗弃在街头     |                                                                                                                                      |
| 5  | have no food or water                                 | 没有吃的，也没有水喝    | or 用于否定句/疑问句， and 用于肯定句                                                                                                              |
| 6  | hungry and thirsty                                    | 又饿又渴          |                                                                                                                                      |
| 7  | take…to…./bring..to.                                  | 把…带走/.….把带来   |                                                                                                                                      |
| 8  | keep as one's pet                                     | 饲养……作为某人的宠物   |                                                                                                                                      |
| 9  | Which one do you like best?                           | 你最喜欢哪一个？      |                                                                                                                                      |
| 10 | prefer the black one                                  | 更喜欢黑色的那一个     | prefer = like ... better                                                                                                             |
| 11 | the yellow and brown one                              | 黄、棕相间的那只      |                                                                                                                                      |
| 12 | look after                                            | 照顾            | = take care of = care for                                                                                                            |
|    | how to take care of..                                 | 如何照顾          |                                                                                                                                      |
|    | look after.….(well)                                   | 照顾好某人         | = take (good) care of                                                                                                                |
| 13 | feed him/her 1-2 times every day                      | 每天喂他/她1\~2次   |                                                                                                                                      |
| 14 | special dog food for puppies                          | 特别的小狗食品       | sth for sb，为某人预备某物                                                                                                                   |
| 15 | a bowl of water to drink.                             | 一碗水喝          | ==sth to do，用来做某动作的某物==                                                                                                              |
| 16 | a blanket to keep him warm                            | 一条用来保暖的毯子     |                                                                                                                                      |
| 17 | hard dog biscuits to chew                             | 用来咀嚼的坚硬的狗饼干   |                                                                                                                                      |
| 18 | a basket to sleep in.                                 | 一个睡觉用的篮子      |                                                                                                                                      |
| 19 | hold her carefully with both hands.                   | 两手小心地抱着它。     | ==留意 hold 的用法和不规则变化==                                                                                                                |
| 20 | take him/her to a park or countryside for a walk      | 把他/她带去公园或乡村散步 |                                                                                                                                      |
| 21 | play with sb.                                         | 和某人一起玩        |                                                                                                                                      |
| 22 | buy sth.for sb.                                       | 为某人买.……       | = buy sb. sh.                                                                                                                        |
| 23 | save animals from danger                              | 挽救动物免受危险      |                                                                                                                                      |
| 24 | be kind/unkind to.…                                   | 对.…友好/不友好     |                                                                                                                                      |
| 25 | help sb (to) do sth.                                  | 帮助某人做某事       |                                                                                                                                      |
| 26 | promise to do                                         | 承诺做某事         |                                                                                                                                      |
| 27 | promise not to do                                     | 承诺不做某事        |                                                                                                                                      |
| 28 | clinics for sick animals                              | 为患病动物诊治的诊所    |                                                                                                                                      |
| 29 | live in caves                                         | 生活在岩洞里        |                                                                                                                                      |
| 30 | guard the caves                                       | 守卫山洞          |                                                                                                                                      |
| 31 | keep people safe from danger                          | 使人们处于安全中不受危险  |                                                                                                                                      |
|    | keep...from…                                          | 保护……免于        | = prevent...from，例句 prevent sb from doing sth                                                                                        |
| 32 | help blind people cross the road safely               | 帮助盲人安全过马路     | help sb do sth                                                                                                                       |
| 33 | in many different ways                                | 在很多不同方面       |                                                                                                                                      |
| 34 | help the police catch thieves and find missing people | 帮警察抓小偷和寻找失踪人员 | help sb do sth                                                                                                                       |
| 35 | on the farm                                           | 在农场           | 农村广袤，需要使用 on                                                                                                                         |
| 36 | should do sth./shouldn't do sth.                      | 应该做某事/不应该做某事  |                                                                                                                                      |

### 词性转换

1.  `friend` (n.)---`friendly` (adj.)友好的 `be firiendly to`
2.  `visit` (v.)--`visitor` (n.) 访客
3.  `lovely` (adj.)--`love` (n./(v.) 爱情/爱恋
4.  `care` (n.)/(v)--`careful` (adj.) `carefully` (adv.) `careless` (adj) 粗心的
5.  `read` (V.)-- `reader`(n.) `reading` (n.)
6.  `save` (v.)--`safe` (adj.) `safely` (adv.) `safety` (n.)
7.  `unkind` (adj.)-- `kind` (adj.) `be (un)kind to` `kindness` (n.)仁慈
8.  `danger` (n.)---`dangerous` (adj.)`be in/out of danger`
9.  `help` (v.)/(n.)-- `helpful`(adj.)`be helpful to` `helper` (n.)帮手,工具
10. `home` (n.)---`homeless` (adj.)无家可归的
11. `cross` (v)-- `across` (prep.)
12. `different` (adj.)-- `difference` (n.)
13. `missing` (adj.)-- `miss` (v.)
14. `farmer` (n.)--- `farm` (n.)

### 语法与句型

| 序号 | English                     | 中文           | 备注                       |
| -- | --------------------------- | ------------ | ------------------------ |
| 1  | Would you like to do sth.?  | 你愿意做…?       | 固定搭配，多用多练                |
| 2  | Which one do you like best? | 你最喜欢哪一个？     |                          |
| 3  | I prefer the one.           | 我较喜欢这个       |                          |
| 4  | Feed/Give/Hold/Play/Take…   | 喂养/给/抓住/玩/带走 | ==动词及介词构成多种不同词义，需要熟练背诵== |

## Unit Three

本单元主要是各个国家和人民之间的词汇和表达方式，请留意背诵。

### 词组与固定搭配

|    | English                        | 中文           | 备注                 |
| :- | :----------------------------- | :----------- | :----------------- |
| 1  | friends from other countries   | 来自其他国家的朋友    |                    |
| 2  | a crowded city                 | 一个拥挤的城市      |                    |
| 3  | for example                    | 例如           |                    |
| 4  | more than=over                 | 超过；多于        |                    |
| 5  | most of them                   | 他们中的大多数      |                    |
| 6  | six million-6000000            | 六百万          |                    |
| 7  | call sb.…                      | 把某人称作        |                    |
| 8  | near                           | 距离.近         | close to 还有许多其他表达法 |
| 9  | far away from                  | 距离…远         |                    |
| 10 | read (sth.) about              | 阅读与.有关的….    |                    |
| 11 | in magazines and newspapers    | 在报纸、杂志上      | 留意介词 in            |
| 12 | know about                     | 知道关于.…的事情    |                    |
| 13 | write (a letter) to            | 给…写信         |                    |
| 14 | in another country             | 在另一个国家       |                    |
| 15 | at school                      | 在上学，在校求学     |                    |
| 16 | make a list                    | 列一个清单        |                    |
| 17 | a list of                      | 一份..的清单      |                    |
| 18 | send sth. to sb./ send sb.sth. | 把.….寄(送)给.   |                    |
| 19 | a photo of my family           | 一张我家人的照片     |                    |
| 20 | write soon                     | 请尽快回信        |                    |
| 21 | in Grade 7                     | 就读于七年级       |                    |
| 22 | junior high school             | 初级中学         |                    |
| 23 | thank sb.for sth./ doing sth.  | 为某事/做某事而感谢某人 |                    |
| 24 | favourite subject              | 最喜欢的科目       |                    |

### 词性转换

1.  `foreigner` n. 外国人 -> `foreign` adj. 外国的 `a foreign language`
2.  `crowded` adj. 拥挤的 -> `crowd` n. 人群
3.  `Canada` n. 加拿大 -> `Canadian` n.& adj.加拿大人:加拿大的 `I am Canadian.` (adj.) 我是加拿大人。相当于`I am from Canada. I am a Canadian.`(n.) 我是一个加拿大人。
4.  `Australia` n. 澳大利亚 -> `Australian` n.& adj.澳大利亚人；澳大利亚(人)的
5.  `India` n. 印度 -> `Indian` n.& adj.印度人:印度(人)的
6.  `Britain` n.英国 -> `Briton` n.英国人 `British` n.& adj.英国人:英国(人)的
7.  `Japan` n. 日本 -> `Japanese` n.& adj. 日本人:日本(人)的
8.  `nationality` n. 国籍 -> `What nationality are you? What's your nationality? I'm Chinese.`
    `nation` 国家，民族
    `national` adj. 国家的，民族的 `the Chinese national flag`
    `international` adj. 国际的 `international flight`
9.  `interest` n. `place of interest` (u)n. 【注】不可数，必须单数 `What are your interests and hobbies?` 【注】可数 ->
    `interested` adj. `The little boy is interested in painting.`
    `interesting` adj. `The film is interesting.` / `It is an interesting film.`
10. `yourself` pron. 你自己 -> `yourselves` pron. 你们自己 `you` pron. 你，你们 `yours` pron. 你的，你们的

【注】

1.  国家、国籍、国民、语言、昵称、钱币等是一大堆词汇，也有许多特例，需要专题讨论。
2.  留意对应的冠词、单复数、语言和国籍形容词之间的变化。不过更多的是用来背诵的。

### 语法与句型

1.  `Over six milion people live in Garden City.` 600多万人居住在花园城。
    a. `over`相当于`more than`,是‘“超过；多于”的意思，常常放在数词的前面。
    【例】`There are over 5000 adjectives in that dictionary.`那本词典中有5000多个形容词。
    b. `six million`即6000000.六百万
    c. 较大数词的读法:
    *   654 `six hundred and fifty-four` 百位数与十位数间要用 `and`
    *   1,718 `one thousand seven hundred and eighteen` 千位数与百位数间无`and`，==需要重点留意==
    *   27,105 `twenty-seven thousand one hundred and five` 千位数前的数字照一位数、两位数或三位数的读法
    *   108,221 `one hundred and eight thousand two hundred and twenty-one`
    *   2,700,560 `two milion seven hundred thousand five hundred and sixty`
        【注】两个“,”由左到右依次代表“百万位”和“千位”，而这些",”前后的数字可按一、二、三位数方法读，在“,”处相应加上单位`million`和`thousand`即可。
        【拓】`billion` 十亿
2.  `What do we call people from Canada?`我们如何称呼来自加拿大的人呢？
    `We call them Canadians.` 我们把他们称为加拿大人。`call sb.sth.` 把某人称作
    【例】`His name is Richard, but we call him Dick.`
3.  国家有关的重点词汇:国名n. 国籍adj. 国民n.。通常国籍与国民是同一个词，但要区别词性。
    国名-国籍-国民(pl.)
    `China-Chinese-Chinese (Chinese)` 中国
    `Canada-Canadian-Canadian (Canadians)` 加拿大
    `America/the USA-American-American (Americans)` 美国
    `Britain/England/the UK-British/English-Briton/Englishman(Britons/Englishmen)` 英国
    `India-Indian-Indian (Indians)` 印度
    `Australia-Australian-Australian (Australians)` 澳大利亚
    `Japan-Japanese-Japanese (Japanese)` 日本
    `Austria-Austrian-Austrian (Austrians)` 奥地利
    `Germany-German-German (Germans)` 德国
    `France-French-Frenchman (Frenchmen)` 法国
    `Thailand-Thai-Thai (Thais)` 泰国
    `Italy-Italian-Italian (Italians)` 意大利
4.  `Britain` n.英国
    表示“英国”的单词有`Britain, England` 和 `the UK`.
    `England`原指英格兰，它是英语四大行政区中最大的一个，所以常用来指英国；`Britain` 指不列颠群岛，包括  `England`(英格兰)、`Scotland`(苏格兰)、和`Wales`(威尔士)三个行政区，因此也常用来表示英国，也称`the Great Britain`; `the UK`是英国国名`the United Kingdom`的缩写；英国全称`the United Kingdom of Great Britain and North Ireland`大不列颠及北爱尔兰联合王国。`the British`常常用于表示“(统称)英国人”
5.  `We can visit countries near or far away from China.`
    我们可以访问那些距离中国或近或远的国家。`near`和`far away from`是一对反义词，表示“距离..近”和“距离..远”
    本句中`near or far away from China`做后置定语修饰`countries`.
6.  `We can also read about them in magazines and newspapers.`
    我们同样可以通过阅读杂志和报纸了解这些国家。
    a. `read about`读到过...;通过阅读了解...
    `I have read about the accident in the factory.`
    b. `read`与`read about`
    `read`表示“阅读”，后接书籍、报刊、杂志。`read about`表示“阅读与..有关的.…”, 相当于`read something about`,后接某个对象或事件。
    c. 表示“在报纸、杂志上”时，介词用`in`而不用`on`
7.  `What nationality are you? /What's your nationality?/Where are you from?` 你是哪国人？
    `I'm Chinese.`
    `nationality` n. 国籍，在回答这个问题时，要使用形容词，如`Chinese, Japanese`等。
8.  `What would you like to know about your penfriend?` 你想了解笔友哪些方面的情况？
    `know`与`know about`
    `I know that man over there.`我认识那边的那个人。
    `Do you know about Jay Chou?`你知道周杰伦吗？
    `know`表示“认识；知道”，而`know about`表示“知道关于..的事情”
9.  `I'd like to know his favorite subjects at school.` 我想知道他上学时最喜欢的学科。
    `at school`是在上学，在求学的意思，`school`前不加`the`。如使用`at the school`，则表示“在学校”的意思，没有求学的含义。
    `We've been friends ever since we met at school.`自从我们上学时认识后就一直是好朋友。
10. `Sex` (M/F) 性别 (男性/女性)
    M代表male男性，F代表female女性
11. `Penfriends International sent your name to my school.` 国际笔友会把你的名字寄到了我校。
    `send--sent--sent`
    `send sth.to sb.` = `send sb.sth.` 把.寄(送)给…
    `Thank you for sending me those cards.`=`Thank you for sending those cards to me.`
12. `Toronto Junior High School` 多伦多初级中学
    `Toronto` n. 多伦多(加拿大港市)
    `junior` adj.初等的；初级的 `junior high school` 初级中学
    `senior` adj.高等的；高级的；年长的 `senior high school` 高级中学
    `primary` adj.初等教育的，小学的 `primary school` 小学
13. `I am in Grade 7.` 我读七年级。
    表示就读于某个班级或年级时，应使用介词`in`。`I'm in Class 2, Grade 7.`
14. 现在完成时`The present perfect tense`
    a. 概念及用法:
    1.表示过去发生的（==或未发生的==）动作对现在造成的影响或结果。==单元重点内容！==常用词:`already,yet,just,ever...before,never`)
    【注】ever用于疑问句，never用于陈述句。
    【例】`Have you ever been to Lhasa?` 你去过拉萨么？
    【例】`I have never been to Lhasa.` 我从未去过拉萨。
    2.表示过去发生的动作一直延续到现在，而且有可能继续延续下去。此用法中要求动词必须是延续性动词。
    常用词:`since` (连接时间点) `for`(连接时间段)
    
    b. 结构:`have/has` + 动词的过去分词(不规则动词的过去分词要专门记忆。)
    *   肯定句:`I have visited the UK.It's far away from China.`
    *   否定句:`I haven't visited the UK yet. `
    *   疑问句:`Have you visited the UK yet? `
    *   `Yes,I have./No,I haven't. `

    c. 动词过去式及过去分词的构成:
        规则变化:
    *   一般情况下在动词词尾直接加-ed. 【例】`jump-jumped-jumped`
    *   以不发音的e结尾的动词直接加-d. 【例】`love-loved-loved`
    *   以辅音字母加y结尾的动词，去y变i+ed；【例】`study-studied-studied`
    *   以重读闭音节结尾，且词尾只有一个辅音字母的动词，双写最后一个辅音字母，再加-ed. 【例】`stop-stopped-stopped`
        不规则变化是重点考试内容，需要专项学习和背诵，参见教材P107
        
    d. `have/has gone to….`和`have/has been to.`的区别:
        `have/has gone to…`某人去了某地(主语不在出发地，去了未回)
        `have/has been to.…`某人去过某地(主语不在目的地，去了已回)
        `have/has been in...`某人已经留在某地(状态维持)
        【例】`He has gone to Beijing.`他去北京了。
        `He has been to Beijing three times.`他去过北京三次了。
        
    e. `have/has been to`和`have/has been in`的区别:
        `have/has been to`强调动作的过程
        `have/has been in`强调动作的状态(通常与表示一段时间的时间状语连用)
        【例】`They have been to Europe once.`
        `They have been in Europe for 10 years`.
        
    f. `already` 和 `yet`
        `already`用于现在完成时的肯定句中，表示已经，可置于句中或句尾；
        `yet`用于否定句或疑问句中，常用于句末。
        【例】`I have already finished my homework.`
        `Have you finished your homework yet?`
        `No, not yet./No,I haven't finished my homework yet.`
        
    g. `since` 和 `for`
        在现在完成时中经常用`since`和`for`连接时间状语，`since`所指的是一个时间点，`for`所指的是一段时间。该用法强调动作的延续性，因此句中的谓语动词必须是延续性动词。
    *   `since` + 时间点:
        `since`作介词 + 时间点(`since` 2002)
        `since` 作连词，引导时间状语从句，从句中动词需用一般过去时。
        【例】`Mark has learned Chinese since 2002.`
        `Mark has learned Chinese since he came to China.`
    *   `for` + 一段时间:
        【例】`Mark has learned Chinese for 7 years.`
    *   `He has learned Chinese for 7 years./ He has learned Chinese since 7 years ago`
        【注】对 `for`及`since`连接的时间状语提问均用`how long`
        `How long has he learned Chinese?`
    *   短暂性动词不可出现在含有`since/for`的现在完成时。如需使用，则要转换为表示延续性状态的动词。
        【例】`His grandfather has died for 5 years.` 应改为:
        `His grandfather has been dead for 5 years.`

## Unit Four

本单元主要是各种职业的积累。

### 词组与固定搭配

|    | English                                        | 中文              | 备注                                       |
| :- | :--------------------------------------------- | :-------------- | :--------------------------------------- |
| 1  | the same….as…                                  | 与…一样            |                                          |
| 2  | make sick people better                        | 使病人更好           | ==让病人恢复得更好==                             |
| 3  | work for…                                      | 为…工作            |                                          |
| 4  | a construction company                         | 一家建筑公司          |                                          |
| 5  | draw plans of buildings                        | 画大楼的设计图         | ==平面图：floor plan==                       |
| 6  | type letters                                   | 打信              |                                          |
| 7  | move sth. to sp.                               | 将(某物)搬到(某地)     |                                          |
| 8  | wear a uniform                                 | 穿一件制服           | ==留意不定冠词==                               |
| 9  | at work                                        | 在工作，在上班         |                                          |
| 10 | drive sb. to sp.                               | 开车送人去某地         |                                          |
| 11 | put out fires                                  | 灭火              |                                          |
| 12 | look after/ take care of / care for            | 照顾              |                                          |
| 13 | deliver letters and parcels                    | 递送信件和包裹         |                                          |
| 14 | in the city center                             | 在市中心            |                                          |
| 15 | take notes                                     | 记笔记             |                                          |
| 16 | go to work                                     | 去上班             |                                          |
| 17 | answer the phone                               | 接电话             |                                          |
| 18 | go to meetings                                 | 参加会议，出席会议       |                                          |
| 19 | do many different things                       | 做许多不同的事情        |                                          |
| 20 | enjoy working with all the people in my office | 喜欢与办公室所有的人在一起工作 |                                          |
| 21 | knock down                                     | 撞倒              | ==knock down是撞倒，而knock into是撞到==         |
| 22 | catch fire                                     | 着火              |                                          |
| 23 | both..and.…                                    | 两者都             |                                          |
| 24 | run away                                       | 逃跑              |                                          |
| 25 | be afraid                                      | 害怕              |                                          |
| 26 | the scene of the accident                      | 事故现场            |                                          |
| 27 | fire engine                                    | 消防车             |                                          |
| 28 | stop the traffic                               | 阻断交通            |                                          |
| 29 | let sb. do sth.                                | 让某人做某事          |                                          |
| 30 | arrive at the hospital                         | 到达医院            |                                          |
| 31 | have a broken arm                              | 手臂骨折            |                                          |
| 32 | two days later                                 | 两天之后            |                                          |
| 33 | take sb. home                                  | 送某人回家           |                                          |
| 34 | on one's way to sp.                            | 在某人去某地的路上       | on my way home / on my way to the office |
| 35 | see sb.doing sth.                              | 看见某人正在做某事       |                                          |
| 36 | buy sth.for sb.                                | 为某人买某物          |                                          |
| 37 | start work early                               | 上班很早            |                                          |
| 38 | empty the rubbish bins                         | 清空垃圾桶           |                                          |

### 词性转换

1.  `different` adj.不同的 -> `difference` n.区别，不同点
2.  `office` n. 办公室 -> `officer` n.官员，职员
3.  `remove` v.移开，搬迁 -> `removal` n. 搬迁，搬移
4.  `quick` adj.快速的 -> `quickly` adv.快速地
5.  `meet` v. 遇见 -> `meeting` n. 会议
6.  `manage` v. 管理 -> `manager` n. 经理
7.  `motorcycle` n. 摩托车 -> `motorcyclist` n. 摩托车手
8.  `break` v. 打破 -> `broken` adj.伤残的，破损的
9.  `engine` n. 发动机，引擎 -> `engineer` n.工程师
10. `clean` v. 打扫 -> `cleaner` n.清洁工
11. `bake` v. 烘烤 -> `baker` n.烤面包师 `bakery` n.面包房

### 语法与句型

句型:
1. `An architect draws plans of buildings.` 建筑师给大楼建筑画设计图。
2. `A removal man moves people's furniture to their new flats.` 搬运工把人们的家居搬入他们的新家。
3. `Afireman puts out fires and rescues people.` 消防员灭火并救人。
4. `An SPCA officer takes care of animals.` SPCA 工作人员照顾动物。
5. `An ambulance worker drives an ambulance.` 救护人员开救护车。
6. `A postman delivers letters and parcels.` 邮递员递送信件和包裹。
7. `Who wears a uniform at work?` 谁在工作时穿制服？
8. `He sees the street cleaners cleaning the streets.` 他看见环卫工人正在清扫街道。

Key points:
1. `the same + n.as` `as + adj. + as`
    【例】`Jack is the same height as Tom.` = `Jack is as tall as Tom.`
    【例】`They are the same age as you and Ben.` = `They are as old as you and Ben.`
2. `work for` 为……效力，为.…工作 【例】`I work for the government.`
    `work in` 在……工作 【例】`She works in a factory.`
    `work as` 担任…….的职务 【例】`She works as a nurse in that hospital.`
3. `move people's furniture to their new flats`
    `move sth.to sp.` 将某物搬到某地
    【例】`Would you please help me move the table to the sitting room?`
    `move to sp.` 搬家去(某地)
    【例】`They have moved to the city centre.`
4. `make sick people better`
    `make sb. + adj.` 使某人怎么样 【例】`The news made her happy.`
    `make sb. do` 使某人做什么【例】`Their words made us laugh.`
    `make sb. + n.` 使某人成为什么【例】`Her parents wanted to make her an artist.`
5. `a uniform / a university / a useful book`
    `an uncle / an umbrella / an underground station`
6. `wear`与`put on`
    `wear`表示穿得状态，而`put on`表示穿的动作。
    【例】`She wears a blue skirt.`
    【例】`You'd better put on your coat. It's very cold outside.`
7. `neighbour` n.邻居(`someone who lives next to you or near you`) 
    【例】`Turn your radio down, or you'll wake the neighbours.` `neighbourhood` n. 住宅区，邻里关系
    【例】`We live in a rich neighbourhood.`
8. `in the lift` 在电梯里 `take a lift / by lift` 乘电梯
9. `go to meetings`=`attend meetings` 出席会议
10. `manage` v. `manager` n.
11. `go to school` 去上学 `go to the school`前往学校(不一定上学)
    `go to hospital` 去看病 `go to the hospital` 到医院去
    `in hospital` 住院 `in the hospital` 在医院(不一定生病住院)
12. `exciting / excited`,`interesting/ interested`,`boring/bored` 对于物和人的形容词
13. `When Ben was walking with Sam in the street,…`
    `when` 当.…时候，引导时间状语从句
    `was walking`过去进行时:
    过去进行时表示过去一个时间点或时间段中正在发生的动作
    构成:`was / were + doing`
    【例】`I was playing football at 10 o'clock yesterday morning.` 
    `When the telephone rang, I was having a shower.`
14. `motorcycle` n. `motorcyclist` n.
    `ist`: `artist` / `cyclist` / `typist` / `pianist` /`motoreyclist` / `scientist` (艺术家，自行车手，打字员，钢琴家，摩托车手，科学家)
15. `engine` n. `engineer` n.
16. `both….and..` 引导前后两个主语时，谓语用复数
17. `hurt` adj.受伤的v.受伤 `hurt-hurt-hurt`
18. `be afraid of sth` 害怕某物本身 `be afraid to do sth` 害怕做某事(而产生某种后果) `I'm afraid` 我恐怕。
19. 叙述事情发展顺序: `first, next, then, finally`
20. `arrive at /in, get to, reach + sp.`到达`reach` 到达
    【例】`They arrived at the school. They arrived.`
21. `break-broke-broken` 
    `broken` adj. `a broken window`
22. 本单元中出现的动词过去式:
    `meet met met, see saw seen `
    `sweep swept swept, buy ,bought bought` 
    `come came come, wear wore worm `
    `drive drove driven, run ran run `
    `make made made, go went gone `
    `catch caught caught, find found found` 
    `take took taken, have had had `
    `sell sold sold, hurt hurt hurt `
    `carry carried carried, stop stopped stopped`
23. `on one's way to...` `on one's way to school` `on one's way home` 
    `home`表示回家时是一个副词，所以前面不需要介词`to` 
    `on one's way to sp.` = `when…` 
    `I can see a lot of people on my way to school.` 
    = `I can see a lot of people when I go to school.`
24. `empty` adj. 空无一物
    【例】`an empty box` `an empty bottle`
    `empty` v. 将里面东西倒掉 `emptied` `emptied` 
    【例】`Did you empty the box?`
25. `see sb. doing sth.` 看见.….正在做…
    `see sb. do sth.`  看见…做过/经常做
26. `early` adv./adj. `earlier` `the earliest`
    adv: 【例】`She went out early in the morning.` `They have to come home early.` 
    adj. 【例】`an afternoon in early spring in the early days`
27. `sell` v. `seller` n.
    `sell sb. sth.` = `sell sth.to sb.`
28. `buy sb sth` = `buy sth for sb`

【注】
由于英语、日语、满洲话对于汉语的侵蚀，汉语的许多语法与英语也有接近趋势，为某人买某件东西，向某人推销某件东西，这些介词并不需要额外记忆。倒是哪些不需要介词的用法需要留意，同时关注不同介词组合后的含义。

## Unit Five

本单元主要是房地产相关的场景和搬家事宜。

### 词组与固定搭配

|    | English                               | 中文           | 备注                            |
| :- | :------------------------------------ | :----------- | :---------------------------- |
| 1  | tidy up                               | 使...整齐       |                               |
| 2  | much bigger                           | 大多了          | much 后更着形容词比较级                |
| 3  | think about doing sth                 | 考虑做某事        |                               |
| 4  | enough space                          | 足够的空间        |                               |
| 5  | a bigger/smaller flat                 | 一个更大/更小的公寓   |                               |
| 6  | the Lis                               | 姓李的一家人(复数)   | +s方式和名词复数一致                   |
| 7  | look for                              | 寻找           | ==留意与 search / find 的区别==     |
| 8  | what kind of…                         | 什么种类的…       |                               |
| 9  | live in the suburbs                   | 住在郊区         | ==suburbs 使用复数==              |
| 10 | estate agency                         | 房产中介公司       |                               |
| 11 | live far away from                    | 住得离开..远      |                               |
| 12 | busy roads                            | 繁忙的街道        |                               |
| 13 | on a ==removal day==                  | 在搬家的那天       |                               |
| 14 | move to                               | 搬家到….        |                               |
| 15 | next to                               | 在….近旁        |                               |
| 16 | opposite the sofa                     | 与沙发相对        |                               |
| 17 | between…and…                          | 在…和…之间       |                               |
| 18 | in front of                           | 在..前面        |                               |
| 19 | the differences between the two flats | 两套房子之间的区别    |                               |
| 20 | ask someone about something           | 关于某事询问某人     |                               |
| 21 | a bedroom with one big window         | 有一个大玻璃窗的一间卧室 | 在英国，房子的窗户数量是上税的，所以有扇大大的窗户非常棒。 |
| 22 | a flat with a kitchen                 | 一套带一间厨房的房子   |                               |
| 23 | too small for us                      | 对我们来说太小了     |                               |
| 24 | an underground station                | 一个地铁站        |                               |

### 词性转换

1. `choose` v.选择 `choice` n.选择
2. `shelf` n.架子 (复数) `shelves`
3. `balcony` n.阳 台(复数) `balconies`
4. `tidy` adj.整洁的 `untidy` adj.不整洁的 `tidy` v.使…整齐 `tidy up`
5. `agent` n.代理人 `agency` n.代理机构
6. `move` v.移动 `removal` n.移动 【例】`a removal man`
7. `live` v.居住 `living` n.生活生存 【例】`living room` 
    `life` n.生活【例】`a happy life`
8. `help` v.帮助 `helpful` adj.乐于助人的 `helper` n.帮手

### 语法与句型

1. My bedroom is too small for all my things.
    `too + adj. + for sb.`对某人而言太(怎么样) 
    【例】`That T-shirt is too big for her.`
    `too + adj./adv. + for sb./sth. to do`对某人而言太(怎么样)而不能(怎么样)
    【例】`Mathematics is too difficult for me to learn.`
    `The bag is too heavy for me to carry.` 
2. `I also need a table for my computer.` `need sth. for + n./ doing sth.` “需要某物来(做某享)” `need`为实义动词结构: 
    - `need sornething` 【例`】We need a lot of practice to keep strong.` 
    - `need to do sth.` 【例】`What do we need to buy for the picnic? `
    【注】否定句/疑问句时由助动词`do/does/did`构成。
3. `Ben and I are much bigger now.`
    much在本句中作副词，修饰比较bigger，表示“…….得多”的意思。
    【例】`The lazy man is much heavier now.`
    常见的可修饰比较级的词有:`much, a little, even, still, far, a lot`等。此外，`much`亦可作形容词，修饰不可数名词
    【例】`much water/ milk`
4. `Let's talk to Dad when he comes home.` 
    和某人谈话 `talk to sb.` 
    谈论某事 `talk about sth.` 
    和某人谈论某事 `talk to sb. about sth.` 
    和某人交谈 `taik with sb.`     
    【例】`Now my teacher is talking to Alice.`
    【例】`Let's talk about our plan for the trip.` 
    【例】`We can talk to our parents about the problem.` 
    【例】`My uncle is very funny. I like to talk with him.`
    【注】此句为主从复合句，主句用祈使句表示一般将来时，从句为`when`引导的时间状语从句需用一般现在时。**主将从现**
    【例】`Don't go outside when it is rainy.` 
5. `What's the matter? `“怎么了有什么问题/发生了什么事啊”。
    可使用`What's the matter with somebody?` 
    同义句型`What's wrong (with somebody)? `
6. `I'd like a flat with../I'd like to live in the suburbs.` 
    `I'd like sth.`=`I want sth` 
    `Id like to do sth.`=`I want to do sth.` 
    【辨】区别:`like`:喜欢搭配 n./`like to do /doing`, `would like`想要搭配n./`to do` 
    【例】`I would like to have a cup of coffee.`
    【例】`I like chocolate and when I went to Belgium I would like to buy a large box.`
7. `We'I look for a bigger flat.` `look for`:“寻找”，强调“寻找”这一动作。`find`: “找到发现”，强调“找”的结果。
    【例】`Did you find Li Ming yesterday? No, we looked for him everywhere, but didn't find him. `
    【例】`I was looking for you this time yesterday, but couldn't find you.`
8. `I'd like a flat with three big bedroom, a big kitchen, two bathrooms and a balcony.`
    `with`:有，带有，对`with`短语进行划线提问，用`Which/What kind of...` ==可能是个冷僻的考点==
    【例】`The Palace Museum is a place with many ancient buildings.`
    【例】`The Great Wall is a long wall with towers on it.`
9. `Can I help you?`
    “有什么我可以帮忙的吗”，服务行业用语，不同场景回答句不同，往往是客人提出自己的要求。
    `Can I help you?`=`May I help you?/What can I do for you? /Anything I can do for you?`
10. `The Lis have moved to their new flat in Water Bay.` `the Lis`=`the Li's family`,指”李先生一家”或“姓李的一家”。
    定冠词`the` 加姓氏复数可以用来表示某一家人(复数)。
    【例】`The Smiths went to Shanghai to visit the World Expo last week.`
11. `Where do you want the round table, Mrs.Li?` 意思接近
    `Where do you want me to put the round table?`
12. `Put it on the floor between the TV set and the sofa, please.` 
    `between`:多指两者之间。【例】 `What's the difference between the two glasses?`
    `among`:指三者或三者以上的同类事物之间，或在一群人或一些物之中，表示“在...中间，在………之中”。 
    【例】`The thief hid himself among those passengers.`
13. `Put it opposite the sofa/on the floor /near the big window`. 方位介词: 
    `on`:“在……上面”，有紧靠在某物之上的意思。
    `near`“靠近；在.…….附近” 【反】`far away from`(在..远处)。`next to`“紧靠；在.…隔壁”【近】`beside`。
    `opposite` “在…….对面；与……面对面”. 
    `between A and B`“在A和B之间”的意思。
    `in front of`“在….….….前面”，【反】是`behind`(在.…..后面。 
    除课文句型`put.….`方位介词可用于`there be`句型，均做地点状语。也可修饰名词做定语。
    【例】`There is a beautiful rug on the floor.`地点状语 
    【例】`The rug on the floor is beautiful.`定语
