#!/usr/bin/env python3
# coding: utf-8

def main():
    for x in range(1,10):
        for y in range(1,10):
            print("{} ".format(x*y, ">4.0f"), end="")
        print("")

if __name__ == "__main__":
    main()
