#!/usr/bin/env python3
#coding:utf-8

import sys
import argparse
import random

def help():
    print("options:\r--source=filename, --count==test_size")

def quiz_run(lines, count=2, unit=None, mode=1):
    tests = []
    if unit is None:
        tests = lines
    else:
        for line in lines:
            if unit in line:
                tests.append(line)
        #print(tests)
        print(len(tests))

    random.shuffle(tests)

    count = min(len(tests), count)

    s = 0
    for i in range(count):
        q = random.choice(tests)
        print(q.strip())
        s += 1
    return(s, count)

def quiz_report():
    pass

def main():
    source = None
    count = None
    unit = None
    mode = 1
    test_size = 0
    test_done = 0
    test_pass = 0
    parser = argparse.ArgumentParser()
    parser.add_argument("-s","--source", help="source url")
    parser.add_argument("-c","--count", help="test count")
    parser.add_argument("-u","--unit", help="unit nr.")
    parser.add_argument("-m","--mode", help="quiz mode, 1: CNToEn, 2: EnToCN")
    args = parser.parse_args()
    if args.source == None:
        help()
        sys.exit(0)
    elif args.count == None or isinstance(args.count, int):
        help()
        sys.exit(0)        
    #print("source:{}".format(args.source))
    #print("count:{}".format(args.count))
    #print("unit:{}".format(args.unit))
    #print("mode:{}".format(args.mode))

    with open(args.source,'r',encoding='utf-8') as f:
        lines = f.readlines()

    quiz_run(lines, count=int(args.count), unit=args.unit, mode=args.mode)
    quiz_report()

if __name__ == "__main__":
    main()
