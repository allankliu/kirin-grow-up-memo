#!/usr/bin/env python3
#coding:utf-8

def main():
    for i in range(1,20):
         print("[{}] pi= {}".format(i, 3.14*i, '>0.2f'))

if __name__ == "__main__":
    main()
