#!/usr/bin/env python3
#coding: utf-8

source = 0
def find_word(line):
    print(line)
    c, w, pron, prop, trans = line.split(' ')
    print("{} {} {} {} {}".format(c, w, pron, prop,trans))

def find_lesson(line):
    global source
    #print("lesson:{}".format(line))
    line = line.replace('>','')
    _,ln = line.split(' ')
    source = "NCE2L".format(ln)
    #print("lesson:{}".format(ln))

def find_empty(line):
    #print("empty")
    pass

def find_error(line):
    print("error:{}".format(line))

def main():
    with open('NCE2words.txt','r',encoding='utf-8') as f:
        lines = f.readlines()
        for l in lines:
            l = l.strip()
            if len(l) == 0:
                find_empty(l)
            elif '[' in l or ']' in l:
                find_word(l)
            elif '>' in l:
                find_lesson(l)
            else:
                find_error(l)

if __name__ == '__main__':
    main()