>>Irregular verbs
be (am,is) was been
become became become
buy bought bought
do did done
eat ate eaten
get got got
go went gone
have had had
say said said
take took taken

>>>G6B
be (am, is,are) was been
be were been
bear bore born
become became blown become
blow blew
bring bought brought bought brought
do buy did done
eat ate eaten
fall fell fallen
fly flew flown
get got got
go went gone
have had had had to
have to had to
lose lost lost
make made made
put put put
say said said
see saw seen
sink sank/sunk sunk
take took taken
write wrote written
