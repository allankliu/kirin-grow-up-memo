>>>Lesson 1
1 private ['praivit] adj. 私人的 
2 conversation [konve'seijen] n. 谈话 
3 theatre ['eiate] n. 剧场，戏院 
4 seat [si:t] n. 座位 
5 play [plei] n. 戏
6 loudly ['laudli] adv. 大声地 
7 angry ['ængri] adj. 生气的 
8 angrily ['ængrili] adv. 生气地 
9 attention [a'ten∫an] n. 注意 
10 bear [bee] v. 容忍 
11 business ['biznis] n. 事 
12 rudely ['ru:dli] adv. 无礼地，粗鲁地 
>>>Lesson 2
1 until [an'til,an'til] prep. 直到 
2 outside [aut'said] adv. 外面 
3 ring [rin] v. (铃、电话等)响 
4 aunt [a:nt] n. 姑，姨，婶，舅妈 
5 repeat [ri'pi:t] v. 重复 
>>>Lesson 3 
1 send [send] v. 寄，送 
2 postcard ['peustka:d] n. 明信片 
3 spoil [spoil] v. 使索然无味，损坏 
4 museum [mju:'ziam] n. 博物馆 
5 public ['p^blik] adj. 公共的 
6 friendly ['frendli] adj. 友好的 
7 waiter ['weite] n. 服务员，招待员 
8 lend [lend] v. 借给 
9 decision [di'sizen] n. 决定 
10 whole [heul] adj. 整个的 
11 single ['singel] adj. 唯一的，单一的 
11 precious ['prejes] adj. 珍贵的
>>>Lesson 4 
1 exciting [ik'saitin] adj. 令人兴奋的
2 receive [ri'si:v] v. 接受，收到
3 firm [fa:m] n. 商行，公司
4 different ['difrent] adj. 不同的
5 centre ['senta] n. 中心
6 abroad [o'bro:d] adv. 在国外
>>>Lesson 5
1 pigeon [pidzin] n. 鸽子
2 message ['mesidz] n. 信息
3 over ['auve] v. 越过
4 distance ['distens] n. 距离
5 request [ri'kwest] n. 要求，请求
6 spare [spea] n. 备件
7 service ['se:vis] n. 业务，服务
>>>Lesson 6
1 beggar ['bege] n. 乞丐
2 food [fu:d] n. 食物
3 pocket ['pokit] n. 衣服口袋
4 call [ko:l] v. 拜访，光顾
>>>Lesson 7
1 detective [di'tektiv] n. 侦探
2 airport ['eepo:t] n. 机场
3 expect [ik'spekt] v. 期待，等待
4 valuable ['valjuebel,-jubal] adj. 贵重的
5 parcel ['pa:sl] n. 包裹
6 diamond ['daiemend] n. 钻石
7 steal [sti:l] v. 偷
8 main [mein] adj. 主要的
9 airfield ['eefi:ld] n. 飞机起落的场地
10 guard [ga:d] n. 警戒，守卫
11 single ['singel] adj. 唯一的，单一的 
11 precious ['prejes] adj. 珍贵的
12 stone [steun] n. 石子
13 sand [send] n. 沙子
>>>Lesson 8
1 competition [,kompi'tifan] n. 比赛,竞赛 
2 neat [ni:t] adj. 整齐的，整洁的 
3 path [pa:0] n. 小路，小径 
4 wooden ['wudn] adj. 木头的 
5 pool [pu:l] n. 水池 
>>>Lesson 9 
1 welcome ['welkem] n. 欢迎 
1 welcome ['welkem] v. 欢迎
2 crowd [kraud] n. 人群 
3 gather ['geee] v. 聚集 
4 hand [hend] n. (表或机器的)指针 
5 shout [faut] v. 腻叫 
6 refuse [ri'fju:z,ri'fju:s] v. 拒绝 
7 laugh [la:f] v. 笑 
>>>Lesson 10 
1 jazz [dzæz] n. 爵士音乐 
2 musical ['mju:zikel] adj. 音乐的 
3 instrument ['instrument] n. 乐器 
4 clavichord ['kleviko:d] n. 古钢琴 
5 recently ['ri:sentli] adv. 最近 
6 damage ['demidz] v. 损坏 
7 key [ki:] n. 琴键 
8 string [strin] n. (乐器的)弦
9 shock [fok,fak] v. 使不悦或生气，震惊 
10 allow [e'lau] v. 允许，让 
11 touch [tatf] v. 触摸 
>>>Lesson 11 
1 turn [te:n] n. 行为，举止 
2 deserve [di'za:v] v. 应得到，值得
3 lawyer ['lo:je] n. 律师 
4 bank [benk] n. 银行 
5 salary ['sxlori] n. 工资 
6 immediately [i'mi:dietli] adv. 立刻 
>>>Lesson 12
1 luck [l^k] n. 运气，幸运
2 captain ['kaptin] n. 船长
3 sail [seil] v. 航行
4 harbour ['ha:be] n. 港口
5 proud [praud] adj. 自豪
6 important [im'po:tent] adj. 重要的
>>>Lesson 13
1 group [gru:p] n. 小组，团体
2 pop singer [] n. 流行歌手
3 club [klab] n. 俱乐部
4 performance [pe'fo:mens] n. 演出
5 occasion [e'keizen] n. 场合
>>>Lesson 14
1 amusing [e'mju:zin] adj. 好笑的，有趣的
2 experience [ik'spieriens] n. 经历
3 wave [weiv] v. 招手
4 lift [lift] n. 搭便车
5 reply [ri'plai] v. 回答
6 language ['lengwidz] n. 语言
7 journey ['dza:ni] n. 旅行
>>>Lesson 15
1 secretary ['sekreteri,-teri] n. 秘书
2 nervous ['ne:ves] adj. 精神紧张的
3 afford [a'fo:d] v. 负担得起
4 weak [wi:k] adj. 弱的
5 interrupt [，inte'rapt] v. 插话，打断
>>>Lesson 16
1 park [pa:k] v. 停放(汽车)
2 traffic ['træfik] n. 交通
3 ticket ['tikit] n. 交通法规罚款单
4 note [nout] n. 便条 
5 area ['eerie] n. 地点 
6 sign [sain] n. 指示牌 
7 reminder [ri'mainde] n. 指示
8 fail [feil] v. 无视，忘记
9 obey [e'bei] v. 服从
>>>Lesson 17 
1 appear [a'pia] v. 登场，扮演 
2 stage [steidz] n. 舞台
3 bright [brait] adj. 鲜艳的 
4 stocking ['stokin] n. (女用)长筒袜 
5 sock [sok] n. 短袜 
>>>Lesson 18 
1 pub [p^b] n. 小酒店
2 landlord ['lendlo:d] n. 店主 
3 bill [bil] n. 帐单 
>>>Lesson 19 
1 hurry ['h^ri] v. 匆忙 
2 ticket office [] n. 售票处 
3 pity['piti] n. 令人遗憾的事 
4 exclaim [ik'skleim] v. 大声说 
5 return [ri'te:n] v. 退回
6 sadly ['sædli] adv. 悲哀地，丧气地 
>>>Lesson 20 
1 catch [kætj] v. 抓到 
2 fisherman['fifemen] n. 钓鱼人，渔民 
3 boot [bu:t] n. 靴子 
4 waste [weist] n. 浪费
5 realize ['rialaiz] v. 意识到 
>>>Lesson 21 
1 mad [med] adj. 发疯 
2 reason ['ri:zon] n. 原因
3 sum [sam] n. 量
4 determined [di'te:mind] adj. 坚定的，下决心的
>>>Lesson 22
1 dream [dri:m] v. 做梦，梦想
2 age[eidz] n. 年龄
3 channel ['tfenel] n. 海峡
4 throw [erou] v. 扔，抛
>>>Lesson 23
1 complete [kem'pli:t] v. 完成
2 modern['moden] adj. 新式的，与以往不同的
3 strange [streind3] adj. 奇怪的
4 district ['distrikt] n. 地区
>>>Lesson 24
1 manager ['mænidze] n. 经理
2 upset [^p'set] adj. 不安
3 sympathetic [.simpo'0etik] adj. 表示同情的
4 complain [kem'plein] v. 抱怨
5 wicked ['wikid] adj. 很坏的，邪恶的
6 contain [ken'tein] v. 包含、内装
7 honesty ['onisti] n. 诚实
>>>Lesson 25
1 railway ['reilwei] n. 铁路
2 porter ['po:te] n. 搬运工
3 several ['severel] 几个
4 foreigner['forine] n. 外国人
5 wonder ['wande] v. 感到奇怪
>>>Lesson 26
1 art[a:t] n. 艺术
2 critic ['kritik] n. 评论家
3 paint [peint] v. 画
4 pretend [pri'tend] v. 假装 
5 pattern ['petn] n. 图案 
6 curtain ['ke:ten] n. 窗帘，幕布
7 material [me'tieriel] n. 材料 
8 appreciate [e'pri:fieit] v. 鉴赏 
9 notice ['nautis] v. 注意到 
10 whether['weee] conj.是否 
11 hang [hen] v. 悬挂，吊 
12 critically ['kritikali] adv. 批评地 
13 upside ['^psaid] 上下颠倒地 
>>>Lesson 27 
1 tent [tent] n. 帐篷 
2 field [fi:ld] n. 田地，田野 
3 smell [smel] v. 闻起来
4 wonderful ['wandeful] adj. 极好的 
5 campfire ['kemp,faia] n. 营火，篝火 
6 creep [kri:p] v. 爬行 
7 sleeping bag ['sli:pin] 睡袋 
8 comfortable ['kamfotebel] adj. 舒适的，安逸的 
9 soundly ['saundli] adv. 香甜地 
10 leap [li:p] v. 跳跃，跳起 
11 heavily ['hevili] adv. 大量地 
12 stream [stri:m] n. 小溪
13 form [fo:m] v. 形成 
14 wind [wind, waind] v. 蜿蜒 
15 right [rait] adv. 正好 
>>>Lesson 28 
1 rare [ree] adj. 罕见的 
2 ancient ['einJent] adj. 古代的，古老的 
3 myth [mie] n. 神话故事 
4 trouble ['trabal] n. 麻烦 
5 effect [i'fekt] n. 结果，效果
6 Medusa [mi'dju:ze] n. 美杜莎(古希腊神话中位蛇发女怪之一) 
7 Gorgon ['go:gen] n. (古希腊神话中的)3位蛇发女怪之一
>>>Lesson 29
1 taxi ['teksi] n. 出租汽车
2 Pilatus Porter [] n. 皮勒特斯·波特(飞机机名)
3 land [lænd] v. 着陆
4 plough [plau] v. 耕地
5 lonely ['launli] adj. 偏僻的，人迹罕至的
6 Welsh [wel5] adj. 威尔士的
7 roof [ru:f] n. 楼顶
8 block [blok] n. 一座大楼
9 flat [flæt] n. 公寓房
10 desert ['dezat] v. 废弃
>>>Lesson 30
1 polo ['pauleu] n. 水球
2 cut [knt] v. 穿过
3 row [reu, rou] v. 划(船)
4 kick [kik] v. 踢
5 towards [te'wo:dz] prep. 朝，向
6 nearly ['niali] adv. 几乎
7 sight [sait] n. 眼界，视域
>>>Lesson 31
1 retire [ri'taie] v. 退休
2 company ['kampani] n. 公司
3 bicycle ['baisik(a)1] n. 自行车
4 save [seiv] v. 积蓄
5 workshop ['we:kfop] n. 车间
6 helper ['helpe] n. 帮手，助手
7 employ [im'ploi] v. 雇佣
8 grandson ['grensan] n. 孙子
>>>Lesson 32
1 once [wans] adv. 曾经，以前
2 temptation [temp'teifen] n. 诱惑 
3 article ['a:tikal] n. 物品，东西 
4 wrap [ræp] v. 包裹 
5 simply ['simpli] adv. 仅仅 
6 arrest [o'rest] v. 逮捕 
>>>Lesson 33 
1 darkness ['da:knis] n. 黑暗 
2 explain [ik'splein] v. 解释，叙述 
3 coast [kaust] n. 海岸
4 storm [sto:m] n. 暴风雨 
5 towards [te'wo:dz] prep. 接近 
6 rock [rok] n. 岩石，礁石 
7 shore [So:] n. 海岸 
8 light [lait] n. 灯光 
9 ahead [o'hed] adv. 在前面 
10 cliff [klif] n. 峭壁 
11 struggle ['str^gl] v. 挣扎 
12 hospital ['hospit] n. 医院 
>>>Lesson 34
1 station ['steifen] n. (警察)局 
2 most [meust] adv. 相当，非常 
>>>Lesson 35 
1 while [wail] n. 一段时间 
2 regret [ri'gret] v. 后悔 
3 far [fa:] adv. 非常 
4 rush [r^j] v. 冲
5 act [ækt] v. 行动 
6 straight [streit] adv. 径直 
7 fright [frait] n. 害怕 
8 battered ['bæted] adj. 撞坏的 
9 shortly ['fo:tli] adv. 很快，不久 
10 afterwards ['a:ftawedz] adv. 以后 
>>>Lesson 36
1 record ['reko:d, ri'ko:d] n. 记录
2 strong [stron] adj. 强壮的
3 swimmer ['swime] n. 游泳运动员
4 succeed [sok'si:d] v. 成功
5 train [trein] v. 训练
6 anxiously ['ænkjasli] adv. 焦急地
7 intend [in'tend] v. 打算
8 solid ['solid] adj. 固体的，硬的
>>>Lesson 37
1 Olympic [e'limpik] adj. 奥林匹克的
2 hold [hould] v. 召开
3 government ['gavenment] n. 政府
4 immense [i'mens] adj. 巨大的
5 stadium ['steidiom] n. 露天体育场
6 standard ['stended] n. 标准
7 capital ['kepitl] n. 首都
8 fantastic [fen'tæstik] adj. 巨大的
9 design [di'zain] v. 设计
>>>Lesson 38
1 except [ik'sept] prep. 除了
2 Mediterranean [.medita'reinion] n. (the~)地中海
3 complain [kem'plein] v. 抱怨
4 continually [ken'tinjuali] adv. 不断地
5 bitterly ['bitali] adv. 刺骨地
6 sunshine ['san[ain] n. 阳光
>>>Lesson 39
1 operation [.ope'reifen] n. 手术
2 successful [sok'sesfal] adj. 成功的
3 following ['foleuin] adj. 下一个
4 patient ['peifant] n. 病人
5 alone [e'loun] adj. 独自的
6 exchange [iks't∫eindz] n. (电话的)交换台
7 inquire [in'kwaie] v. 询问，打听 
8 certain ['se:ten] adj. 某个 
9 caller ['ko:le] n. 打电话的人 
10 relative ['relativ] n. 亲戚 
>>>Lesson 40 
1 hostess ['heustis] n. 女主人 
2 unsmiling ['^n'smailin] adj. 不笑的，严肃的 
3 tight [tait] adj. 紧身的 
4 fix [fiks] v. 疑视 
5 globe [gleub] n. 地球 
6 despair [di'spee] n. 绝望 
>>>Lesson 41 
1 rude [ru:d] adj. 无礼的
2 mirror ['mire] n. 镜子 
3 hole [haul] n. 孔 
4 remark [ri'ma:k] v,评说 
5 remind [ri'maind] v. 提醒 
6 lighthouse ['laithaus] n. 灯塔 
>>>Lesson 42 
1 musical ['mju:zikel] adj. 精通音乐的 
2 market ['ma:kit] n. 市场，集市 
3 snake [sneik] n. 玩蛇者(通常借音乐控制) 
4 pipe [paip] n. (吹奏地)管乐器
5 tune [tju:n, tu:n] n. 曲调 
6 glimpse [glimps] n. 一瞥 
7 snake [sneik] n. 蛇 
8 movement ['mu:vment] n. 动作 
9 continue [kon'tinju:] v. 继续 
11 obviously ['obviesli] adv. 显然 
10 dance [da:ns] v. 跳舞 
12 difference ['difrens] n. 差别
13 Indian ['indien] adj. 印度的 
>>>Lesson 43
1 pole [peul] n. (地球的)极
2 flight [flait] n. 飞行
3 explorer [ik'splo:re] n. 探险家
4 lie [lai] v. 处于
5 serious ['siarias] adj. 严重的
6 point [point] n. 地点
7 seem [si:m] v. 似乎，好像
8 crash [kræ∫] v. 坠毁
9 sack [sæk] n. 袋子
10 clear [klie] v. 越过
11 aircraft ['eekra:ft] n. 飞机
12 endless ['endlis] adj. 无尽的
13 plain [plein] n. 平原
>>>Lesson 44
1 forest ['forist] n. 森林
2 risk [risk] n. 危险，冒险
3 picnic ['piknik] n. 野餐
4 edge [edz] n. 边缘
5 strap [stræp] n. 带，皮带
6 possession [pa'zejan] n. 所有
7 breath [breθ] n. 呼吸
8 contents ['ko:ntents] n. (常用复数)内有的物品
9 mend [mend] v. 修理
>>>Lesson 45
1 clear [klia] adj. 无罪的，不亏心的
2 conscience ['konjens] n. 良心，道德心
3 wallet ['wa:lit] n. 皮夹，钱夹
4 savings ['seivinz] n. 存款
5 villager ['vilidze] n. 村民
6 percent [pa'sent] 百分之...
>>>Lesson 46
1 unload [^n'laud] v. 卸(货)
2 wooden ['wudn] adj. 木制的 
3 extremely [ik'stri:mli] adv. 非常，极其 
4 occur [ə'ke:] v. 发生 
5 astonish [e'stoni5] v. 使惊讶 
6 pile [pail] n. 堆 
7 woollen ['wulen] n. 羊毛的 
8 goods [gudz] n. (常用复数)货物。商品 
9 discover[dis'kave] v. 发现 
10 admit [ed'mit] v. 承认 
11 confine [ken'fain] v. 关在(一个狭小的空间里)
12 normal ['no:mel] adj. 正常的，通常的 
>>>Lesson 47 
1 thirsty ['0ə:sti] adj. 贪杯的 
2 ghost [geust] n. 鬼魂 
3 haunt [ho:nt] v. (鬼)来访，闹鬼
4 block [blok] v. 堵 >>>Lesson 51
5 furniture ['fe:nit/e] n. 家具 
6 whisky ['wiski] n. 威士忌酒 
7 suggest [so'dzest] v. 暗示 
8 shake [feik] v. 摇动 
9 accept [ak'sept] v. 接受 
>>>Lesson 48 
1 pull [pul] v. 拔 
2 cotton ['kotn]药棉 
3 collect [ke'lekt] v. 搜集 
4 collection [ke'lek∫ən] n. 收藏品，收集品
5 nod [nod] v. 点头 >>>Lesson 52
6 meanwhile ['mi:n'wail] adv. 同时 
>>>Lesson 49 
1 tired ['taiad] adj. 厌烦的 
2 real [riel] adj. 真正的
3 owner ['aune] n. 主人 
4 spring [sprin] n. 弹簧 
5 mattress ['mætris] n. 床垫
6 gust [gast] n. 一阵风
7 sweep [swi:p] v. 扫，刮
8 courtyard ['ko:tja:d] n. 院子
9 smash [smæf] v. 碰碎，摔碎
10 miraculously [mi'rekjulesli] adv. 奇迹般地
11 unhurt [an'he:t] adj. 没有受伤的
12 glance [gla:ns] v. 扫视
13 promptly ['promptli] adv. 迅速地
>>>Lesson 50
1 ride [raid] n. 旅行
2 excursion [ik'ske:en] n. 远足
3 conductor [kan'd^kte] n. 售票员
4 view [vju:] n. 景色
>>>Lesson 51
1 reward [ri'wo:d] n. 报偿
2 virtue ['ve:t∫u:] n. 美德
3 diet ['daiet] n. 节食
4 forbid [fe'bid] v. 禁止
5 hurriedly ['h^ridli] adv. 匆忙地
6 embarrass [im'bæres] v. 使尴尬
7 guiltily ['giltili] adv. 内疚地
8 strict [strikt] adj. 严格的
9 reward [ri'wo:d] v. 给奖赏
10 occasionally [a'keizeneli] adv. 偶尔地
>>>Lesson 52
1 temporarily ['tempererili] adv. 暂时地
2 inch [intj] n. 英寸(度量单位)
3 space [speis] n. 空间
4 actually ['æktfuali] adv. 实际上
>>>Lesson 53
1 hot [hot] adj. 带电的，充电的
2 fireman ['faiemon] n. 消防队员
3 cause [ko:z] v. 引起 n. 原因 
4 examine [ig'zemin] v. 检查 
5 accidentally [eksi'denteli] ad. 意外地,偶然地 
6 remains [ri'meinz] n. 尸体，残骸 
7 wire ['waie] n. 电线 
8 volt [veult] n. 伏特(电压单位) 
9 power line [] n. 电力线 
10 solve [solv] v. 解决 
11 mystery ['misteri] n. 谜 
12 snatch [snætf] v. 抓住 
13 spark [spa:k] n. 电火花 
>>>Lesson 54
1 sticky ['stiki] adj. 粘的 
2 finger ['finge] n. 手指 
3 pie [pai] n. 馅饼 
4 mix [miks] v. 混合，拌和 
5 pastry ['peistri] n. 面糊 
6 annoying [a'noiin] adj. 恼人的 
7 receiver [ri'si:ve] n. 电话的话筒 
8 dismay [dis'mei] v. 失望，泄气 
9 recognize['rekegnaiz] v. 认出，听出 
10 persuade [pe'sweid] v. 说服，劝说 
11 mess [mes] n. 乱七八糟 
12 doorknob ['do:nob] n. 门把手 
13 sign [sain] v. 签字
14 register ['redziste] v. 挂号邮寄 
>>>Lesson 55 
1 gold [gould] n. 金子 
2 mine [main] n. 矿 
3 treasure ['treze] n. 财宝 
4 revealer [ri'vi:le] n. 探测器 
5 invent [in'vent] v. 发明 
6 detect [di'tekt] v. 探测 
7 bury ['beri] v. 埋藏
8 cave [keiv] n. 山洞
9 seashore ['si:jo:] n. 海岸
10 pirate ['paieret] n. 海盗
11 arm [a:m] v. 武装
12 soil [soil] n. 泥土
13 entrance ['entrens] n. 人口
14 finally ['faineli] adv. 最后
15 worthless ['we:0les] adj. 毫无价值的
16 thoroughly ['0^reli] adv. 彻底地
17 trunk [trank] n. 行李箱
18 confident ['konfidont] adj. 有信心的
19 value ['vælju:] n. 价值
>>>Lesson 56
1 sound [saund] n. 声音
2 excitement [ik'saitmont] n. 激动，兴奋
3 handsome ['hendsem] adj. 漂亮的，美观的
4 Rolls-Royce ['reulz'rois] 罗尔斯-罗伊斯
5 Benz [] n. 奔驰
6 wheel [wi:l] n. 轮子
7 explosion [ik'splauzan] n. 爆炸，轰响
8 course [ko:s] n. 跑道;行程
9 rival ['raivel] n. 对手
10 speed [spi:d] v. 疾驶
11 downhill [.daun'hil] adv. 下坡
>>>Lesson 57
1 madam ['medem] n. (对妇女的尊称)太太，夫人
2 jeans [dzi:nz] n. 牛仔裤
3 hesitate ['heziteit] v. 犹豫，迟疑
4 serve [se:v] v. 接待(顾客)
5 scornfully ['sko:nfuli] adv. 轻蔑地
6 punish ['p^nif] v. 惩罚
7 fur [fe:] n. 裘皮
8 eager ['i:ge] adj. 热切的，热情的
>>>Lesson 58 
1 blessing ['blesin] n. 福分，福气 
2 disguise [dis'gaiz] n. 伪装 
3 tiny ['taini] adj. 极小的 
4 possess [pe'zes] v. 拥有 
5 cursed ['ke:sid] adj. 可恨的 
6 increase [in'kri:s, 'inkri:s] v. 增加 
7 plant [pla:nt] v. 种值 
8 church [tfə:tj] n. 教堂 
9 evil ['i:vel] adj. 坏的 
10 reputation [repju'teifan] n. 名声 
11 claim [kleim] v. 以...为其后果 
12 victim ['viktim] n. 受害者。牺牲品 
13 vicar ['vike] n. 教区牧师 
14 source [so:s] n. 来源 
15 income ['inkam] n. 收人 
16 trunk [trank] n. 树干 
>>>Lesson 59
1 bark [ba:k] v. 狗叫 
2 press [pres] v. 按压 
3 paw [po:] n. 脚爪 
4 latch [læt∫] n. 门闩 
5 expert ['ekspe:t] n. 专家 
6 develop [di'velap] v. 养成 
7 habit ['hebit] n. 习惯 
8 remove [ri'mu:v] v. 拆掉，取下 
>>>Lesson 60 
1 future ['fju:tfe] n. 未来，前途 
2 fair [fee] n. 集市 
3 fortune-teller ['fo:t∫en'tele] n. 算命人 
4 crystal ['kristal] n. 水晶 
5 relation [ri'leifen] n. 亲属 
6impatiently [im'peifentli] adv. 不耐烦地 
>>>Lesson 61
1 Hubble ['habl] n. 哈勃
2 telescope ['teliskaup] n. 望远镜
3 launch [lo:nt/] v. 发射
4 space [speis] n. 空间
5 NASA ['nese] 国家航空和宇宙航行局
6 billion ['biljen] n. 10亿
7 faulty ['fo:lti] adj. 有错误的
8 astronaut ['estrano:t] n. 宇航员
9 shuttle ['$^tl] n. 宇航飞机
10 Endeavour [in'deve] n. “奋进”号
11 robot-arm [] n. 机器手
12 grab [græb] v. 抓
13 atmosphere ['etmesfie] n. 大气层
14 distant ['distentadj. 谣远的
15 galaxy ['gelekesi] n. 星系
16 universe ['ju:nive:s] n. 宇宙
17 eagle eye [] n. 鹰眼
>>>Lesson 62
1 control [ken'treul] n. 控制
2 smoke [smeuk] n. 烟
3 desolate ['desoleit] adj. 荒凉的
4 threaten ['0retn] v. 威胁
5 surrounding [se'raundin] adj. 周围的
6 destruction [di'strakjen] n. 破坏，毁灭
7 flood [flnd] n. 洪水，水灾
8 authority [o:'0oriti] n. (常用复数)当局
9 grass-seed ['gra:s'si:d] n. 草籽
10 spray [sprei] v. 喷撒
11 quantity ['kwontati] n. 量
12 root [ru:t] n. 根
13 century ['sentferi] n. 世纪
14 patch [pætj] n. 小片
15 blacken ['bleken] v. 变黑，发暗
>>>Lesson 63 
1 circle ['sa:kal] n. 圈子 
2 admire [ed'maie] v. 赞美，钦佩 
3 close [kleuz] adj. 亲密的 
4 wedding ['wedin] n. 婚礼 
5 reception [ri'seplen] n. 执行会 
6 sort [so:t] n. 种类 
>>>Lesson 64 
1 tunnel ['tanl] n. 隧道 
2 port [po:t] n. 港口 
3 ventilate ['ventileit] v. 通风 
4 chimney ['tfimni] n. 烟囱 
5 sea level [] n. 海平面 
6 double ['d^bel] adj. 双的 
7 ventilation [.venti'leifen] n. 通风 
8 fear [fie] v. 害怕 
9 invasion [in'veizen] n. 入侵，侵略 
10 officially [ə'fifeli] adv. 正式地 
11 connect [ke'nekt] v. 连接
12 European [juara'pi:an] adj. 欧洲的 
13 continent['kontinent] n. 大陆 
>>>Lesson 65 
1 versus ['ve:ses] prep. 对 
2 Christmas ['krismes] n. 圣诞节 
3 circus ['sa:kas] n. 马戏团 
4 present pri'zent, 'prezent] n. 礼物 
5 accompany [e'kampeni] v. 陪伴，随行 
6 approach [e'praut/] v. 走近 
7 ought [o:t] modal verb 应该 
8 weigh [wei] v. 重 
9 fortunate ['fo:tJenet] adj. 幸运的 
>>>Lesson 66 
1 Lancaster ['laenkeste] n. 兰开斯特
2 bomber ['bome] n. 轰炸机
3 remote [ri'maut] adj. 偏僻的
4 Pacific [pe'sifik] n. 太平洋
5 damage ['dæmid] v. 毁坏
6 wreck [rek] n. 残骸
7 rediscover [ri:di'skave] v. 重新发现
8 aerial ['eerial] adj. 航空的
9 survey ['so:vei] n. 调查
10 rescue ['reskju:] v. 营救
11 package ['pekidz] v. 把…打包
12 enthusiast [in'0ju:ziæst] n. 热心人
13 restore [ri'sto:] v. 修复
14 imagine [i'mædzin] v. 想像
15 packing ['pækin] 包装箱
16 colony ['kolani] n. 群
17 bee [bi:] n. 蜂
18 hive [haiv] n. 蜂房
19 preserve [pri'za:v] v. 保护
20 beeswax ['bi:zwæks] n. 蜂蜡
>>>Lesson 67
1 volcano [vol'keineu] n. 火山
2 active ['æktiv] adj. 活动的
3 Kivu [] n.  基伍湖
4 Congo ['kongou] n. (the~)刚果
5 Kituro [] n. 基图罗
6 erupt [i'rapt] v. (火山)喷发
7 violently ['vaielentli] adv. 猛烈地，剧烈地
8 manage ['menidz] v. 设法
9 brilliant ['brilient] adj. 精彩的
10 liquid ['likwid] adj. 液态的
11 escape [i'skeip] v. 逃脱
12 alive [e'laiv] adj. 活着的
>>>Lesson 68
1 persistent [pe'sistent] adj. 坚持的，固执的
2 avoid [e'void] v. 避开 
3 insist [in'sist] v. 坚持做 
>>>Lesson 69 
1 murder ['me:de] n. 谋杀 
2 instruct [in'strakt] v. 命令，指示 
3 acquire [e'kwaie] v. 取得，获得 
4 confidence ['konfidans] n. 信心 
5 examiner [ig'zæmine] n. 主考人
6 suppose [se'peuz] v. 假设 
7 tap [tep] v. 轻敲 
8 react [ri'ækt] v. 反应 
9 brake [breik] n. 刹车 
10 pedal ['pedel] n. 踏板 
11 mournful ['mo:nful] a,悲哀的 
>>>Lesson 70 
1 bullfight ['bulfait] n. 斗牛
2 drunk [drank] n. 醉汉 
3 wander ['wonde, 'wa:n:dor] v. 溜达，乱走 
4 ring [rin] n. 圆形竞技场地 
5 unaware [^ne'wee] adj. 不知道的，示觉察的 
6 bull [bul] n. 公牛 
7 matador ['mætedo:] n. 斗牛士 
8 remark [ri'ma:k] n. 评论:言语 
9 apparently [e'perentli] adv. 明显地 
10 sensitive ['sensitiv] adj. 敏感的 
11 criticism ['kritisizem] n. 批评 
12 charge [tja:dz] v. 冲上去
13 clumsily ['klamzili] adv. 笨拙地 
14 bow [bau] v. 鞠躬 
15 safety ['seifti] n. 安全地带 
16 sympathetically [.simpe'0etikeli] adv. 同情地 
>>>Lesson 71 
1 parliament ['pa:lement] n. 议会，国会
2 erect [i'rekt] v. 建起
3 accurate ['ækjuret] adj. 准确的
4 official [e'fifal] n. 官员，行政人员
5 Greenwich ['grinidz] n. 格林威治
6 observatory [ab'ze:vateri] n. 天文台
7 check [tfek] v. 检查
8 microphone ['maikrefeun] n. 扩音器，麦克风
9 tower ['taue] n. 塔
>>>Lesson 72
1 racing ['reisin] n. 竞赛
2 per [po:] prep. 每
3 Utah ['ju:ta:] n. 犹他(美国州名)
4 horsepower ['ho:s.paua] n. 马力
5 burst [be:st] v. 爆裂
6 average ['evaridz] adj. 平均的
7 footstep ['futstep] n. 足迹
>>>Lesson 73
1 record-holder ['reko:d'houlda]纪录保持者
2 truant ['tru:ent] n. 逃学的孩子
3 unimaginative [.ani'madzinativ] adj. 缺乏想象力的
4 shame [feim] n. 惭愧，羞耻
5 hitchhike ['hitjhaik] v. 搭便车旅行
6 meantime ['mi:ntaim] n. 其间
7 lorry ['lori] n. 卡车
8 border ['bo:de] n. 边界
9 evade [i'veid] v. 逃避，逃离
>>>Lesson 74
1 limelight [laimlait] n. 舞台灯光
2 precaution [pri'ko:fen] n. 预防措施
3 fan [fen] n. 狂热者，迷
4 shady ['feidi] adj. 遮荫的
5 sheriff ['ferif] n. 司法长官
6 notice ['neutis] n. 告示
7 sneer [snia] n. 冷笑 
>>>Lesson 75 
1 thick[0ik] adj. 厚的 
2 signal ['signal] n. 信号 
3 stamp [stæmp] v. 跺，踩 
4 helicopter ['helikopte] n. 直升飞机
5 scene [si:n] n. 现场 
6 survivor [se'vaive] n. 幸存者 
>>>Lesson 76 
1 fool [fu:1] n. 傻瓜 
2 bulletin ['bulatin] n. 亲闻简报 
3 announcer [e'naunse] n. (电视、电台)播音员 
4 macaroni [ make'rauni] n. 通心面，空心面条 
5 leading ['li:din] adj. 主要的 
6 grower ['greue] n. 种植者 
7 splendid ['splendid] adj. 极好的 
8 stalk [sto:k] n. 梗 
9 gather ['geee] v. 收庄稼 
10 thresh [0re5] v. 打(庄稼)
11 process['preuses] v. 加工 
12 signor ['si:njo:] n. (意大利语)先生 
13 present [pri'zent, 'prezant] adj. 目前的 
14 champion ['tfampien] n. 冠军 
15 studio ['stju:diou] n. 播音室 
>>>Lesson 77 
1 mummy ['mami] n. 木乃伊
2 Egyptian [i'dzipen] adj. 埃及的 
3 temple ['tempel] n. 庙 
4 mark [ma:k] n. 斑点 
5 plate [pleit] n. (照相)底片 
6 disease [di'zi:z] n. 疾病 
7 last [la:st] v. 持续 
8 prove [pru:v] v. 显示出 
9 resin ['rezin] n. 树脂
10 skin [skin] n. 皮，皮肤
11 section ['sek∫en] n,切片
12 figure ['fige] n. (人的)体形;人像
13 normally ['no:moli] adv. 通常地
14 survive [se'vaiv] v. 幸免于
>>>Lesson 78
1 entitle [in'taitl] v. 以..为名
2 calm [ka:m] v. 使镇定
3 nerve [ne:v] n. 神经
4 concentration [.konsen'treifen] n. 集中。专心
5 suffer ['s^fa] v. 受苦，受害
6 symptom ['simptem] n. 症状
7 temper ['tempe] n. 脾气
8 appetite ['æpitait] n. 胃口，食欲
9 produce [pre'dju:s, 'prodju:s] v. 拿出
10 urge [ə:dz] v. 力劝，怂恿
11 satisfaction [.sætis'fkjen] n. 满意，满足
12 delighted [di'laitid] adj. 欣喜的
>>>Lesson 79
1 parent ['peerent] n. 父(母)亲
2 flight attendant ['flait o.tendont] 空中乘务员
3 frightened ['fraitnd] adj. 害怕，担惊
4 curious ['kjuerias] adj. 急于了解，好奇的
5 bomb [bom] n. 炸弹
6 plant [pla:nt] v. 安放
>>>Lesson 80
1 palace ['pelis] n. 宫殿
2 extraordinary [ik'stro:dineri] adj. 不平常的.非凡的
3 exhibition [eksi'bif(e)n] n. 展览
4 iron ['aian] n. 铁
5 various ['veories] adj. 各种各样的
6 machinery [me'fi:neri] n. 机器
7 display [di'splei] n. 展览 
8 steam [sti:m] n. 蒸汽 
9 profit ['profit] n. 利润 
10 college ['kolidz] n. 学院 
>>>Lesson 81
1 prisoner ['prizene] n. 囚犯 
2 bush [buf] n. 灌木丛 
3 rapidly ['repidli] adv. 迅速地 
4 uniform ['ju:nifo:m] n. 制服 
5 rifle ['raifl] n. 来福枪，步枪 
6 shoulder ['foulde] n. 肩 
7 march [ma:tf] v. 行进 
8 boldly ['beuldli] adv. 大胆地 
9 blaze [bleiz] v. 闪耀 
10 salute [se'lu:t] v. 行礼 
11 elderly ['eldali] adj. 上了年纪的 
12 grey [grei] adj. 灰白的 
13 sharp [Sa:p] adj. 猛烈的
14 blow [blou] n. 打击 
>>>Lesson 82 
1 monster ['monste] n. 怪物 
2 sailor ['seile] n. 海员 
3 sight [sait] v. 见到 
4 creature ['kri:t/a] n. 动物，生物 
5 peculiar [pi'kju:lie] adj. 奇怪的，不寻常的 
6 shining ['fainin] adj. 闪闪发光的 
7 oarfish ['o:fif] n. 桨鱼 
>>>Lesson 83 
1 election [i'lekfen] n. 选举 
2 former ['fo:me] adj. 从前的 
3 defeat [di'fi:t] v. 打败
4 fanatical [fe'netikel] adj. 狂热的 
5 opponent [e'pounant] n. 反对者，对手
6 radical ['rædikel] adj. 激进的
7 progressive [pre'gresiv] adj. 进步的
8 ex- [eks] prefix. (前缀，用于名词前)前...
9 suspicious [se'spifes] adj. 怀疑的
>>>Lesson 84
1 strike [straik] n. 罢工
2 busman ['basmon] n. 公共汽车司机
3 state [steit] v. 正式提出，宣布
4 agreement [e'gri:ment] n. 协议
5 relieve [ri'li:v] v. 减轻
6 pressure ['preja] n. 压力，麻烦
7 extent [ik'stent] n. 程度
8 volunteer [.volen'tie] v. 自动提出，自愿
9 gratitude ['gretitju:d] n. 感激
10 Press [pres] n. 新闻界
11 object ['obdzikt. eb'dzekt] v. 不赞成，反对
>>>Lesson 85
1 inform [in'fo:m] v. 告诉，通知
2 headmaster [，hed'ma:ste] n. 校长
3 contribute [ken'tribju:t] v. 招助。援助
4 gift [gift] n. 礼物，赠品
5 album ['ælbem] n. 签名簿，相册
6 patience ['peifens] n. 耐心
7 encouragement [in'karidzmant] n. 鼓励
8 farewell [fea'wel] n. 告别
9 honour ['one] n. 敬意
10 coincidence [keu'insidans] n. 巧合
11 total ['teutl] n. 总数
12 devote [di'veut] v. 致力于
13 gardening ['ga:danin] n. 园艺
14 hobby ['hobi] n. 爱好，嗜好
>>>Lesson 86
1 swing [swin] v. 转向 
2 speedboat ['spi:dbeut] n. 快艇 
3 desperately ['desparitli] adv. 绝望地 
4 companion [kem'penien] n. 同伙，同伴 
5 water ski [] (由快艇牵引水橇)滑水 
6 buoy [boi] n. 浮标 
7 dismay [dis'mei] n. 沮丧 
8 tremendous [tri'mendes] adj. 巨大的
9 petrol ['petral] n. 汽油 
10 drift [drift] v. 漂动，漂流 
11 gently ['dzentli] adv. 缓慢地，轻轻地 
>>>Lesson 87 
1 alibi ['celibail] n. 不在犯罪现场 
2 commit [ke'mit] v. 犯(罪、错) 
3 inspector [in'spekte] n. 探长 
4 employer [im'ploia] n. 雇主 
5 confirm [ken'fe:m] v. 确认，证实 
6 suggest [sa'dzest] v. 提醒 
7 truth [tru:0] n. 真相 
>>>Lesson 88 
1 trap [træp] v. 陷人，使陷于困境 
2 surface ['se:fis] n. 地面。表面 
3 explosive [ik'splausiv] n. 炸药
4 vibration [vai'breijen] n. 震动 
5 collapse [ke'laps] v. 坍塌 
6 drill [dril] v. 钻孔 
7 capsule ['kæpsju:l] n. 容器 
8 layer ['lele] n. 层 
9 beneath [bi'ni:0] prep. 在.之下 
10 lower ['leue] v. 放下，降低
11 progress ['praugres, pre'gres] v. 进展，进行
12 smoothly ['smu:eli] adv. 顺利地 
>>>Lesson 89 
1 slip [slip] n. 小错误
2 comedy ['komidi] n. 喜剧
3 present [pri'zent, 'prezant] v. 演出 adj. 出席，到场的
4 queue [kju:] v. 排队
5 dull [dal] adj. 枯燥，无味
6 artiste [a:'ti:st] n. 艺人
7 advertiser ['ædvetaize] n. 报幕员
>>>Lesson 90
1 chip [t5ip] n. 油煎豆片
2 overfish [euve'fif] v. 过度捕捞
3 giant ['dzaiont] adj. 巨大的
4 terrify ['terifai] v. 吓，使恐怖
5 diver ['daive] n. 潜水员
6 oil rig ['oil,rig] 石油钻塔
7 wit[wit] n. (复数)理智，头脑
8 cage [keidz] n. 笼
9 shark [ja:k] n. 鲨鱼
10 whale [weil] n. 鲸
11 variety [vo'raieti] n. 品种
12 cod [kod] n. 鳕
13 skate [skeit] n. 鳐
14 factor ['fekte] n. 因素
15 crew [kru:] n. 全体工作人员
>>>Lesson 91
1 balloon [be'lu:n] n. 气球
2 royal ['roial] adj. 皇家
3 spy [spai] v. 侦察
4 track [trek] n. 轨迹，踪迹
5 binoculars [bi'nokjulaz] n. 望远镜
>>>Lesson 92
1 fast [fa:st] adv. 熟(睡)
2 ladder ['lade] n. 梯子
3 shed [fed] n. 棚子
4 sarcastic [sa:'kæstik, sar-] adj. 讽刺的，讥笑的
5 tone [teun] n. 语气，腔调 
>>>Lesson 93 
1 noble ['neubel] adj. 高尚的，壮丽的 
2 monument ['moniument] n. 纪念碑
3 statue ['stæt/u:] n. 雕像 
4 liberty ['libati] n. 自由 
5 present [pri'zent, 'prezont] v. 赠送 
6 sculptor ['sk^lpte] n. 雕刻家 
7 actual ['ektfuel] adj. 实际的，真实的
8 copper ['kope] n. 铜
9 support [se'po:t] v. 支持，支撑
10 framework ['freimwe:k] n. 构架，框架
11 transport [tren'spo:t] v. 运送
12 site [sait] n. 场地
13 pedestal ['pedistl] n. 底座
>>>Lesson 94
1 instruct [in'strakt] v. 指导。传授
2 Los Angeles [los'ændzili:z] 洛杉矶
3 reluctant [ri'laktant] adj. 勉强的，不愿意的
4 weight [weit] n. 重物
5 underwater [,^nde'wo:te] adj. 水下的
6 tricycle ['traisikel] n. 三轮车
7 compete [kem'pi:t] v. 比赛，对抗
8 yard [ja:d] n. 码
9 gasp [ga:sp] v. 喘气
>>>Lesson 95
1 fantasy ['fentesi] n. 幻想故事
2 ambassador [am'baesede] n. 大使
3 frightful ['fraitfal] adj. 可怕的，令人吃惊的
4 fire extinguisher [] 灭火器
5 drily ['draili] adv. 冷淡地，枯燥无味地
6 embassy ['embesi] n. 大使馆
7 heaven ['heven] n. 天，天堂
8 basement ['beisment] n. 地下室
9 definitely ['definitli] adv. 肯定地
10 post [peust] v. 派任
11 shot [fot] n. 子弹
>>>Lesson 96
1 festival ['festival] n. 节日
2 lantern ['lænton] n. 灯笼
3 spectacle ['spektekel] n. 景象，壮观，场面
