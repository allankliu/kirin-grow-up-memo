#!/usr/bin/env python3
#coding: utf-8

c_grade = 0
c_module = 0
c_unit = 0
wcount = 0

def find_empty(line):
    pass

def find_unit(line):
    l = line.replace('>','')
    #print(l)
    x = line.split(' ')
    u = x[1]
    n = l[len('Unit x'):]
    return(u,n)

def find_module(line):
    l = line.replace('>','')
    #print(l)
    x = line.split(' ')
    m = x[1]
    n = l[len('Module x'):]
    return(m,n)

def find_grade(line):
    l = line.replace('>','')
    #print(l)
    x = line.split(' ')
    g = x[1]
    n = l[len('Grade '):]
    return(g,n)

def find_word(line):
    _word, _translation = line.split('=')
    return(_word, _translation)

def find_error(line):
    print("ERROR: {}".format(line))

def lineparser(line):
    global wcount
    global c_grade, c_module, c_unit
    line = line.strip()
    _word = _pronounciation = _property = _translation = _comment = _source = None
    if '>>' in line:
        if 'Unit' in line:
            c_unit, n = find_unit(line)
            #print("U[{}]\t{}".format(c_unit, n))
        elif 'Module' in line:
            c_module, n = find_module(line)
            #print("M[{}]\t{}".format(c_module, n))
        elif 'Grade' in line:
            c_grade, n = find_grade(line)
            #print("G[{}]\t{}".format(c_grade, n))
    elif '=' in line:
        w,t = find_word(line)
        wcount += 1
        print('"{}",,,"{}",,OXE{}M{}U{}'.format(w,t,c_grade,c_module,c_unit))
    elif len(line)==0:
        find_empty(line)
    else:
        find_error(line)
        # a test error branch
    return(_word, _pronounciation,_property,_translation,_comment,_source)

def lineprocessor(_word, _pronounciation,_property,_translation,_comment,_source):
    l = ''
    return l

def main():
    nls = []
    _word = None
    _pronounciation = None,
    _property = None,
    _translation = None,
    _comment = None,
    _source = None

    with open('SHOXEG12345words.txt','r',encoding="utf-8") as f:
        lines = f.readlines()
        for l in lines:
            #print(l)
            _word, _pronounciation,_property,_translation,_comment,_source = lineparser(l)
            if _word is None:
                continue
            nl = lineprocessor(_word, _pronounciation,_property,_translation,_comment,_source)
            nls+=nl

if __name__=="__main__":
    main()