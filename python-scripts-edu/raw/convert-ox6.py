#!/usr/bin/env python3
#coding: utf-8

################################
# There is a bug in the script
################################

import sys
import os

c_grade = 0
c_module = 0
c_unit = 0
wcount = 0

def find_empty(line):
    pass

def find_word(line):
    pre,post = line.split('.')
    #print(post)
    x = pre.split(' ')
    p = x[-1]
    w = pre.split(p)[0][:-1]
    p = x[-1] + '.'
    t,s = post[1:].split(' ')  
    return(w,p,t,s)

def find_error(line):
    error = True
    line = line()
    print("ERROR: {}".format(line))

def find_mark(line):
    global c_grade
    #print("MARK: {}".format(line))
    if ("OXE" in line):
        line = line.replace('>','')
    c_grade = line

def lineparser(line):
    global wcount
    global c_grade, c_module, c_unit
    line = line.strip()
    _word = _pronounciation = _property = _translation = _comment = _source = None

    if len(line) == 0:
        #print("empty----->")
        pass
    elif '>' in line:
        find_mark(line)
    elif '.' in line and ' ' in line:
        w,p,t,s = find_word(line)
        src = "{}{}".format(c_grade,s)
        print('"{}",,"{}","{}",,"{}"'.format(w,p,t,src))
    else:
        find_error(line)
    return(_word, _pronounciation,_property,_translation,_comment,_source)

def lineprocessor(_word, _pronounciation,_property,_translation,_comment,_source):
    l = ''
    return l

def main():
    nls = []
    _word = None
    _pronounciation = None,
    _property = None,
    _translation = None,
    _comment = None,
    _source = None

    with open('SHOXEG6Bwords.txt','r',encoding="utf-8") as f:
        lines = f.readlines()
        for l in lines:
            #print(l)
            _word, _pronounciation,_property,_translation,_comment,_source = lineparser(l)
            if _word is None:
                continue
            nl = lineprocessor(_word, _pronounciation,_property,_translation,_comment,_source)
            nls+=nl

if __name__=="__main__":
    main()
