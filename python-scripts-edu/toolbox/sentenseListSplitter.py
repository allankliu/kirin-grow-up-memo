#!/usr/bin/env python3
#coding: utf-8

import sys   
import random 

def main():   
    # 获取文本路径   
    text_path = sys.argv[1]    
    with open(text_path, 'r', encoding='utf-8') as file:       
        lines = file.readlines()    
    for line in lines:       
        sentence = line.strip()       
        if sentence:  # 过滤掉空行           
            words = sentence.split()           
            random.shuffle(words)           
            new_sentence = " ".join(words)           
            print(new_sentence) 
            
if __name__ == "__main__":   
    main()