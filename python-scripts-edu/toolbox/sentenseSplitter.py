#!/usr/bin/env python3
#coding: utf-8

import random 

def main():   
    sentence = input("请输入一个英语句子：")   
    words = sentence.split()   
    random.shuffle(words)   
    new_sentence = " ".join(words)   
    print(new_sentence) 

if __name__ == "__main__":   
    main()
