#!/usr/bin/env python3
#coding:utf-8
# https://www.cnblogs.com/chengxuyonghu/p/13773727.html

import re
import collections
import sys

# Load raw titles
print("Ancient Chinese High Freq Characters and Sources")
ts = []
with open("titles.txt","r",encoding="utf-8") as f:
    lines = f.readlines()
    for l in lines:
        t = re.findall("《.*?》",l)
        for ti in t:
            ts.append(ti)
            #print(ti)

# Reduce titles as unique list
title_minilist = list(set(ts))
title_minilist.sort()
for t in title_minilist:
    #print("{}".format(t))
    pass

# Get frequencies of titles
print("Sources and Frequencies")
for t in title_minilist:
    f = ts.count(t)
    if f > 10:
        print("{},{}".format(t,f))
    else:
        print("{},{}".format(t,f))  
