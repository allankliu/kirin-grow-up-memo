#!/usr/bin/env python3 
#coding: utf-8

import csv

def getOptions():
    pass

def main():
    with open('english.csv', encoding='utf-8') as f:
        reader = csv.reader(f)
        header = next(reader)
        print(header)
        for row in reader:
            print(row)

if __name__ == "__main__":
    getOptions()
    main()
