# Kirin-grow-up-memo

This is an open source yet personl memo for raising up my kid. The purpose is a demonstration for mangement of fragment knowledgement and involving more people in interaction of essential education.

Perferred language: Chinese and English.

- added python-scripts-edu, python scripts for educational purposes

## Educational Purposes

There are various requirements for the essential educations, including but not limited on:

1. Alphabeta list and memorization training for ancient Chinese and modern English.
2. Special helper scripts for pop-up quiz and raw data processing, such as orgnization and index.
3. Special helper for crossword, population with a leading letter, sentense shuttle.
4. Reference coding base for kid programming.

## Submodule Management

Recently I added some book reading memos as submodules. The management of submodules is located at :

https://blog.csdn.net/chenli_001/article/details/125783937

The project is also a base of learning how to use git submodule.

https://blog.csdn.net/hongxingabc/article/details/104930042

## Cross Generator

- https://github.com/riverrun/genxword
- https://pypi.org/project/genxword/


## Handwriting Generatar

- https://github.com/xxNull-lsk/Copybook
- https://www.jianshu.com/p/ddd1bcbebdbf
- https://zhuanlan.zhihu.com/p/39934824
- https://juejin.cn/post/7118635049779462157

## Chinese Pinyin Generator

- https://zhuanlan.zhihu.com/p/76597038